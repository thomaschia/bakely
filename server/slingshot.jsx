import { Meteor } from 'meteor/meteor';

import '../imports/api/slingshot.jsx';

Slingshot.createDirective("imageUploads", Slingshot.S3Storage, {
  region: "ap-southeast-1",
  bucket: Meteor.settings.BucketName,
  acl: "public-read",

  authorize: function() {
    return true;
  },

  key: function(file, fileName) {
    return this.userId + "/temp/" + (new Date()).getTime() + "-" + fileName.imageName;
  }
});
