import { Meteor } from 'meteor/meteor';

import { Bakeshop } from '../imports/api/collections/bakeshop.js';
import { Products } from '../imports/api/collections/products.js';
import { Messages } from '../imports/api/collections/messages.js';
import { Threads } from '../imports/api/collections/threads.js';
import '../imports/api/methods.jsx';
import '../imports/api/slingshot.jsx';

Meteor.startup(() => {
    process.env.MAIL_URL = "smtp://" + Meteor.settings.DefaultSMTPLogin + ":" + Meteor.settings.MailGunPword + "@smtp.mailgun.org:587";
    Accounts.emailTemplates.resetPassword.html = (user, url) => {
      SSR.compileTemplate('resetPassword', Assets.getText('resetPasswordEmail.html'));
      let data = {link: Meteor.settings.AppRoot + '/reset-password/' + url.substring(url.lastIndexOf('/') + 1)};
      //return user.username + " " + Meteor.settings.AppRoot + '/reset-password/' + url.substring(url.lastIndexOf('/') + 1);
      return SSR.render('resetPassword', data);
    }
    WebApp.connectHandlers.use(function(req, res, next) {
      res.setHeader('Access-Control-Allow-Origin', '*');
      return next();
    })
});


Bakeshop._ensureIndex( { name: 1 } );
Meteor.users._ensureIndex( { username: 1});

Meteor.publish('currentUsers', function(search) {
  let query = {};

  if (search) {
    let regex = new RegExp( search, 'i' );
    query = { username: regex };
  }
  return Meteor.users.find(query, {fields: {username: 1}});
})

Meteor.publish('bakeshop', (search, limitNumber) => {
  let query = {};
  let projection = {};

  if (search) {
    let regex = new RegExp( search, 'i' );
    query = { name: regex };
  }

  if (limitNumber) {
    projection = { limit: limitNumber }
  }
  return Bakeshop.find(query, projection);
});


Meteor.publish('products', (search, limitNumber, sortBy, readyIn) => {
  let query = {};
  let projection = {};

  if (search) {
    let regex = new RegExp( search, 'i' );
    query = {name: regex};
  }

  if (sortBy) {
    switch(sortBy) {
      case 'A - Z':
      projection.sort = {name: 1}
      break;
      case 'Z - A':
      projection.sort = {name: -1}
      break;
      case '$ High - Low':
      projection.sort = {price: -1}
      break;
      case '$ Low - High':
      projection.sort = {price: 1}
      break;
    }
  }

  if (readyIn) {
    switch(readyIn) {
      case 'All':
      if (query.readyIn) {
        delete query.readyIn;
      }
      break;
      case '0 - 24h':
      query.readyIn = '0 - 24h';
      break;
      case '1 - 3 days':
      query.readyIn = '1 - 3 days';
      break;
      case 'More than 3 days':
      query.readyIn = 'More than 3 days';
      break;
    }
  }

  if (limitNumber) {
    projection.limit = limitNumber;
  }
  return Products.find(query, projection);
})
