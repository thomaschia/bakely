import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { browserHistory } from 'react-router';


import NaviBar from '../components/Navibar.jsx';
import SearchBar from '../components/SearchBar.jsx';

export default class MainNav extends TrackerReact(Component) {

  constructor() {
    super()

    this.toggleMenu = this.toggleMenu.bind(this);

    this.state = {
      menuHidden: false,
    }
  }

  goHome() {
    browserHistory.push('/');
  }

  toggleMenu() {
    let menuToggled = this.state.menuHidden;
    this.setState({
      menuHidden: !menuToggled
    })
  }

  render() {

    return (
      <div className="main-layout">
        <div className='home-banner-container'>
          <div className='home-banner'>
            <div className='logo-and-menu-button'>
              <div className='banner-logo'>
                <img className="main-page-logo" onClick={this.goHome.bind(this)} src='/images/MainLogo/Bakely_Logo_Large.png' />
              </div>
              <div className='menu-button-container'>
                <div className='menu-button' onClick={this.toggleMenu}>☰</div>
              </div>
            </div>
            <NaviBar menuHidden={this.state.menuHidden} />
          </div>
        </div>
        <div className='main-layout-children-container'>
          <SearchBar />
          <br />
          {this.props.children}
        </div>
      </div>
    )
  }
}
