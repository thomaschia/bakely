import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { browserHistory } from 'react-router';


//import NaviBar from '../components/Navibar.jsx';
//import SearchBar from '../components/SearchBar.jsx';

export default class Main extends TrackerReact(Component) {

  goHome() {
    browserHistory.push('/');
  }

  render() {

    return (
      <div className="main-layout-container">
        <div className='main-div'>
          <img className="main-page-logo" onClick={this.goHome.bind(this)} src='/images/MainLogo/Bakely_Logo_Large.png' />
        </div>
        {this.props.children}
      </div>
    )
  }
}
