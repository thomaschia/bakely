import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { browserHistory } from 'react-router';

export default class Main extends TrackerReact(Component) {

  goHome() {
    browserHistory.push('/');
  }

  render() {

    return (
      <div className="main-layout">
        <div className='home-banner-container'>
          <div className='home-banner'>
            <div className='logo-and-menu-button'>
              <div className='banner-logo-no-nav'>
                <img className="main-page-logo" onClick={this.goHome.bind(this)} src='/images/MainLogo/Bakely_Logo_Large.png' />
              </div>
            </div>
          </div>
        </div>
        <div className='main-layout-children-container'>
          {this.props.children}
        </div>
      </div>
    )
  }
}
