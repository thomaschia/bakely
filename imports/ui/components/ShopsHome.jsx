//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

//UI RELATED IMPORTS
  //COMPONENTS
  import BakeShop from './BakeShop.jsx';

//COLLECTIONS
import { Bakeshop } from '../../api/collections/bakeshop.js';

export default class ShopsHome extends TrackerReact(Component) {

  constructor(props) {
    super(props)

    this.bound_scrollBottom = this.handleScroll.bind(this);

    this.state = {
      infNumber: 10
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.bound_scrollBottom);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.bound_scrollBottom);
  }
  /*
  componentWillUpdate() {
    var node = ReactDOM.findDOMNode(this);
    this.shouldScrollBottom = node.scrollTop + node.offsetHeight === node.scrollHeight;
  }

  componentDidUpdate() {
    if (this.shouldScrollBottom) {
      var node = ReactDOM.findDOMNode(this);
      node.scrollTop = node.scrollHeight;
    }
  }
  */
  handleScroll(event) {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      let showing = this.state.infNumber;
      this.setState({
        infNumber: showing + 5
      })
    }
  }

  shops() {
    return Bakeshop.find({visible: true}).fetch();
  }

  render() {

    let searchQuery = this.props.query? this.props.query: "";

    let bakeshopSub = Meteor.subscribe('bakeshop', searchQuery, this.state.infNumber);
    /* This is causing the page to jump to the top every time the sub limit is changed
    if (!bakeshopSub.ready()) {
      return (<div className='spinner-container-absolute'><div className='spinner'></div></div>)
    }
    */

    let shops = this.shops().map(shop => {
      return <BakeShop key={shop._id} shop={shop} />
    })

    return (
      <div>
        <ReactCSSTransitionGroup
          transitionName='shopshome'
          transitionEnterTimeout={300}
          transitionLeaveTimeout={300}>
          {shops}
        </ReactCSSTransitionGroup>
      </div>
    )
  }
}
