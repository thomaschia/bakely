//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //BOOTSTRAP
  import Button from 'react-bootstrap/lib/Button';
  import Col from 'react-bootstrap/lib/Col';
  import FormControl from 'react-bootstrap/lib/FormControl';
  import FormGroup from 'react-bootstrap/lib/FormGroup';
  import Glyphicon from 'react-bootstrap/lib/Glyphicon';
  import InputGroup from 'react-bootstrap/lib/InputGroup';

  //COMPONENTS
  import ShopsHome from './ShopsHome.jsx';

//COLLECTIONS

//METHODS

export default class Search extends TrackerReact(Component) {

  constructor() {
    super()

    this.state = {
      searchQuery: ""
    }
  }

  searching(event) {
    event.preventDefault();
    this.setState({
      searchQuery: ReactDOM.findDOMNode(searchInput).value
    })
  }

  render() {

    return (
      <div>
        <Col xs={10} xsOffset={1} sm={4} smOffset={4}>
          <FormGroup controlId="searchInput">
            <InputGroup>
              <FormControl type="text" value={this.state.searchQuery} onChange={this.searching.bind(this)} />
              <InputGroup.Button>
                <Button><Glyphicon glyph="search" /></Button>
              </InputGroup.Button>
            </InputGroup>
          </FormGroup>
          <hr />
        </Col>
        <ShopsHome query={this.state.searchQuery} />
      </div>
    )
  }
}
