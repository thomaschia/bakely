import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { browserHistory } from 'react-router';

import Form from 'react-bootstrap/lib/Form';
import FormControl from 'react-bootstrap/lib/FormControl';
import FormGroup from 'react-bootstrap/lib/FormGroup';

import { Bakeshop } from '../../api/collections/bakeshop.js';

import { sendContactMessage } from '../../api/methods.jsx';

export default class ContactPage extends TrackerReact(Component) {

  constructor() {
    super()

    this.handleSend = this.handleSend.bind(this);

    this.state = {
      subscription: {
        bakeshop: Meteor.subscribe('bakeshop')
      },
      subject: '',
      message: '',
    }
  }

  componentWillUnmount() {
    this.state.subscription.bakeshop.stop();
  }

  writingSubject(event) {
    event.preventDefault();
    this.setState({
      subject: event.target.value
    })
  }

  writingMessage(event) {
    event.preventDefault();
    this.setState({
      message: event.target.value
    })
  }

  handleSend() {
    if (!Meteor.userId()) {
      Bert.alert('Log in to send a message', 'danger');
    } else {
      let subject = this.state.subject.trim();
      let message = this.state.message.trim();
      sendContactMessage.call({
        to: this.props.params.id,
        subject: subject,
        message: message
      }, (error) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          Bert.alert('Success', 'success');
          //browserHistory.goBack();
        }
      })
    }
  }

  render() {

    let shop = Bakeshop.findOne({_id: this.props.params.id});

    if (!shop) {
      return <div>Loading</div>
    }

    return (
      <div>
        <div className='send-message-container'>
          <p>To: {shop.name}</p>
          <Form>
            <FormGroup controlId='subject'>
              Subject
              <FormControl type='text' value={this.state.subject} onChange={this.writingSubject.bind(this)} placeholder='Subject' />
            </FormGroup>
            <FormGroup controlId='message'>
              Message
              <FormControl componentClass='textarea' value={this.state.message} onChange={this.writingMessage.bind(this)} placeholder='Message' />
            </FormGroup>
          </Form>
          <div className='send-mail-button-container' onClick={this.handleSend}>
            <p>Send</p>
          </div>
        </div>
      </div>
    )
  }
}
