//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

//UI RELATED IMPORTS
  //Bootstrap
  import Col from 'react-bootstrap/lib/Col';
  import Panel from 'react-bootstrap/lib/Panel';

  //Components
  import Message from './Message.jsx';

//COLLECTIONS
//import { Threads } from '../../api/collections/threads.js';
//import { Messages } from '../../api/collections/messages.js';

export default class MessagePane extends TrackerReact(Component) {

  componentDidUpdate() {
    let self = this;
    let node = ReactDOM.findDOMNode(self);
    node.scrollTop = node.scrollHeight;
  }

  render() {
    {/*
    let msgSub = Meteor.subscribe('messages');
    let userSub = Meteor.subscribe('currentUsers');

    if (!msgSub.ready() || !userSub.ready()) {
      return (<div>Loading...</div>)
    }

    let threadMessages = Messages.find({thread: this.props.thread}, {sort: {timestamp: 1}}).fetch();

    let threadMessagesMap = threadMessages.map(message => {
      return <Message key={message._id} message={message} />
    })
    */}
    return (
      {/*<div id='msgPane'>
        <ReactCSSTransitionGroup
          transitionName='transIn'
          transitionEnterTimeout={200}
          transitionLeaveTimeout={200}>
          {threadMessagesMap}
        </ReactCSSTransitionGroup>
      </div>*/}
    )
  }
}
