//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //COMPONENTS
  import ShopsHome from './ShopsHome.jsx';

//COLLECTIONS
import { Bakeshop } from '../../api/collections/bakeshop.js';

export default class Home extends TrackerReact(Component) {

  render() {

    let query = {};

    return (
      <div>
        <ShopsHome />
      </div>
    )
  }
}
