//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import classNames from 'classnames';
import { browserHistory } from 'react-router';

//UI RELATED IMPORTS
  //BOOTSTRAP
  import Glyphicon from 'react-bootstrap/lib/Glyphicon';

  //COMPONENTS

//METHODS
import { getDistinctProducts } from '../../api/methods.jsx';

//COLLECTIONS
import { Products } from '../../api/collections/products.js';

export default class SearchBar extends TrackerReact(Component) {

  constructor() {
    super()

    this.searching = this.searching.bind(this);
    this.clearText = this.clearText.bind(this);

    this.state = {
      searchQuery: '',
      searchResults: [],
    }
  }

  clearText() {
    this.setState({
      searchQuery: ''
    })
  }

  goToSearch(query, event) {
    event.preventDefault();
    this.setState({
      searchQuery: ''
    })
    browserHistory.push('/search-products/' + query)
    //FlowRouter.go('/search-products/' + query);
  }

  searching(event) {
    let query = event.target.value;
    this.setState({
      searchQuery: query
    })
    this.searchProducts(query);
  }

  searchProducts(query) {
    getDistinctProducts.call({
      searchQuery: query
    }, (error, result) => {
      if (error) {
        console.log(error);
      } else {
        this.setState({
          searchResults: result
        })
      }
    })
  }

  render() {

    let showSearch = this.state.searchQuery? 'show-search' : '';

    let searchClass = classNames('search-results', showSearch);

    let searchResultsUnmapped = this.state.searchResults? this.state.searchResults : [];

    let searchResults = searchResultsUnmapped.map((result, index) => {
      return <li key={index} onClick={this.goToSearch.bind(this, result._id)}>{result._id}</li>
    });

    return (
      <div className='search-bar'>
        <form action='' onSubmit={this.goToSearch.bind(this, this.state.searchQuery)}>
          <input className='main-search-input' ref='searchInput' type='text' value={this.state.searchQuery} onChange={this.searching} placeholder='Search' />
          {this.state.searchQuery? <img src='/images/delete.png' alt='delete-icon' onClick={this.clearText} /> : <div className='search-bar-spacer' /> }
          <button type='submit' className='glyph-div'><Glyphicon className='magnify-glyph' glyph='search' /></button>
        </form>
        <div className={searchClass}>
          <ul>
            {searchResults}
            <li>Search for shops containing {this.state.searchQuery}</li>
          </ul>
        </div>
      </div>
    )
  }
}
