//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

//UI RELATED IMPORTS
  //Bootstrap
  import Col from 'react-bootstrap/lib/Col';
  import Well from 'react-bootstrap/lib/Well';

  //Components

//METHODS
import { messageRead } from '../../api/methods.jsx';

//COLLECTIONS
//import { Threads } from '../../api/collections/threads.js';
//import { Messages } from '../../api/collections/messages.js';

export default class Message extends TrackerReact(Component) {

  componentDidMount() {

    if (this.props.message.from != Meteor.userId()) {
      messageRead.call({
        msgId: this.props.message._id,
      }, (error) => {
        if (error) {
          //handle error
        } else {
          //handle success
        }
      });
    }

  }

  render() {

    let read = (this.props.message.read && this.props.message.to != Meteor.userId())? <p id='read-receipt'>Read</p> : null;

    let msgPull = (this.props.message.from == Meteor.userId())? 'message-panel-right' : 'message-panel-left';

    return (
      <div id={msgPull}>
        <Well bsSize='small'>
          <p>{this.props.message.message}</p>
          <ReactCSSTransitionGroup
            transitionName='transIn'
            transitionEnterTimeout={200}
            transitionLeaveTimeout={0}>
            {read}
          </ReactCSSTransitionGroup>
        </Well>
      </div>
    )
  }
}
