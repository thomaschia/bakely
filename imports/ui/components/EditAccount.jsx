//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //BOOTSTRAP
  import Button from 'react-bootstrap/lib/Button';

  //COMPONENTS
  import RegLogin from './RegLogin';
  import EditAccountModal from './EditAccountModal.jsx';

export default class EditAccount extends TrackerReact(Component) {

  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      currentlyEditing: null
    };
  }

  open(toEdit) {
    this.setState({
      showModal: true,
      currentlyEditing: toEdit
    });
  }

  close() {
    this.setState({
      showModal: false,
    });
  }

  render() {

    var breakStyle = {
      lineHeight: '300%'
    };

    let currentUser = Meteor.userId();

    if (!currentUser) {
      return (<RegLogin />)
    }

    if (!Meteor.user()) {
      return (<div>Loading...</div>)
    }

    return (
      <div className='edit-account'>
        <EditAccountModal show={this.state.showModal} onHide={this.close.bind(this)} editing={this.state.currentlyEditing} />
        Username: {Meteor.user().username}&emsp;<Button onClick={this.open.bind(this, 'username')}>Edit</Button><br style={breakStyle}/>
        Email: {Meteor.user().emails[0].address}&emsp;<Button onClick={this.open.bind(this, 'email')}>Edit</Button><br style={breakStyle}/>
        Password&emsp;<Button onClick={this.open.bind(this, 'password')}>Edit</Button>
      </div>
    )
  }
}
