//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';

//UI RELATED IMPORTS
  //Bootstrap
  import Button from 'react-bootstrap/lib/Button';
  import Col from 'react-bootstrap/lib/Col';
  import Form from 'react-bootstrap/lib/Form';
  import FormControl from 'react-bootstrap/lib/FormControl';
  import FormGroup from 'react-bootstrap/lib/FormGroup';

  //Components

//METHODS
import {forgotLogin} from '../../api/methods.jsx';

//COLLECTIONS

export default class ForgotLogin extends Component {

  constructor() {
    super()

    this.state = {
      isLoading: false
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({
      isLoading: true
    })
    let email = ReactDOM.findDOMNode(forgotEmail).value.trim();
    forgotLogin.call({
      email: email
    }, (error) => {
      this.setState({
        isLoading: false
      })
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        browserHistory.push('/');
        //FlowRouter.go('home');
      }
    })
  }

  render() {

    let spinner = this.state.isLoading? <div className='spinner-container'><div className='spinner'></div></div> : '';

    return (
      <div className='forgot-login-div'>
        {spinner}
        <Form horizontal onSubmit={this.handleSubmit.bind(this)}>
          <Col xs={8} sm={6} md={4} lg={2} xsOffset={2} smOffset={3} mdOffset={4} lgOffset={5}>
            <p>An email will be sent to you with instructions on resetting your login details</p>
            <hr />
          </Col>
          <FormGroup controlId="forgotEmail">
            <Col xs={8} sm={6} md={4} xsOffset={2} smOffset={3} mdOffset={4}>
              <FormControl type="email" placeholder="Email" />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col xs={8} sm={6} md={4} lg={2} xsOffset={2} smOffset={3} mdOffset={4} lgOffset={5}>
              <Button type="submit">Send</Button>
            </Col>
          </FormGroup>
      </Form>
      </div>
    )
  }
}
