//BASIC IMPORTS//
import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { browserHistory } from 'react-router';

//UI RELATED IMPORTS
  //BOOSTRAP
  import Button from 'react-bootstrap/lib/Button';
  import Col from 'react-bootstrap/lib/Col';
  import Glyphicon from 'react-bootstrap/lib/Glyphicon';
  import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
  import Popover from 'react-bootstrap/lib/Popover';

//METHODS
import { deleteProduct } from '../../api/methods.jsx';
import { deleteImages } from '../../api/methods.jsx';
import { toggleInvisible } from '../../api/methods.jsx';

export default class ProductEdit extends TrackerReact(Component) {

  deleteProduct() {
    deleteImages.call({
      array: [this.props.product.image]
    }, (error, result) => {
      if (error) {
        console.log(error);
        Bert.alert(error.reason, 'danger');
      }
    });
    deleteProduct.call({
      productId: this.props.product._id
    }, (error, result) => {
      if (error) {
        console.log(error);
        Bert.alert(error.reason, 'danger');
      }
    });
  }

  editProduct() {
    toggleInvisible.call({
      id: this.props.product._id,
      visibility: false,
    }, (error) => {
      if (error) {
        console.log('Product will be visible during edit');
      }
    })
    browserHistory.push(`/edit-product/${this.props.product._id}`);
    //FlowRouter.go('/edit-product/'+this.props.product._id);
  }

  render() {

    const deletePopover = (
      <Popover id="deletePopover">
        <p>Are you sure you want to delete {this.props.product.name}?</p>
        <Button bsStyle='danger' onClick={this.deleteProduct.bind(this)}>Delete</Button>
      </Popover>
    );

    return (
      <div className="products-container">
        <Col xs={8}>
            <p>{this.props.product.name}</p>
            <p>${this.props.product.price}</p>
        </Col>
        <div className="products-edit-btn">
          <OverlayTrigger trigger="click" rootClose placement="top" containerPadding={10} overlay={deletePopover}>
            <Button bsStyle='danger'><Glyphicon glyph="minus-sign" /></Button>
          </OverlayTrigger>&emsp;
          <Button onClick={this.editProduct.bind(this)}><Glyphicon glyph="pencil" /></Button>
        </div>
      </div>
    )
  }
}
