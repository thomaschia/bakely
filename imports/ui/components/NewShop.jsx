//BASIC IMPORTS//
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { browserHistory } from 'react-router';

//UI RELATED IMPORTS//
  //BOOTSTRAP
  import Alert from 'react-bootstrap/lib/Alert';
  import Button from 'react-bootstrap/lib/Button';
  import Col from 'react-bootstrap/lib/Col';
  import FormControl from 'react-bootstrap/lib/FormControl';
  import FormGroup from 'react-bootstrap/lib/FormGroup';

  //COMPONENTS

//METHODS
import { deleteShop } from '../../api/methods.jsx';
import { insertNewShop } from '../../api/methods.jsx';

//COLLECTION IMPORTS//
import { Bakeshop } from '../../api/collections/bakeshop.js';

export default class NewShop extends Component {

  constructor(props) {
    super(props)

    this.state = {
      name: this.props.shop.name,
      description: this.props.description,
      nameLength: this.props.shop.name.length,
      descLength: this.props.shop.description.length,
    }
  }

  cancel() {
    let shop = Bakeshop.findOne({owner: Meteor.userId()});
    if (shop) {
      deleteShop.call({}, (error) => {
        if (error) {
          Bert.alert('Error during cleanup - please contact us', 'danger');
        } else {
          browserHistory.push('/my-account');
          //FlowRouter.go('my-account')
        }
      });
    } else {
      browserHistory.push('/my-account');
      //FlowRouter.go('my-account');
    }
  }

  nextStep() {
    insertNewShop.call({
      name: this.state.name,
      description: this.state.description
    }, (error, data) => {
      if (error) {
        Bert.alert(error, 'danger');
      } else {
        browserHistory.push(`/new-shop-image/${data}`);
        //FlowRouter.go('/new-shop-image/' + data);
      }
    });
  }

  writingName(event) {
    event.preventDefault();
    this.setState({
      name: event.target.value,
      nameLength: event.target.value.trim().length
    })
  }

  writingDesc(event) {
    event.preventDefault();
    this.setState({
      description: event.target.value,
      descLength: event.target.value.trim().length
    })
  }

  render() {

    let nameWarning = (this.state.nameLength > 40)? <Alert bsStyle='danger'>Exceeded max length</Alert> : "";
    let descWarning = (this.state.descLength > 150)? <Alert bsStyle='danger'>Exceeded max length</Alert> : "";

    let nextState = (this.state.nameLength > 40 || this.state.nameLength == 0 || this.state.descLength == 0 || this.state.descLength > 150)? true : false;

    return (
      <div>
        <Col  xs={10} xsOffset={1} sm={6} smOffset={3}>
          <FormGroup controlId="new-product-name">
            <p>Name:</p>
            <FormControl type="text" value={this.state.name} onChange={this.writingName.bind(this)} />
          </FormGroup>
          <p id="new-product-paragraph">{this.state.nameLength}/40</p>
          {nameWarning}
          <hr />
          <FormGroup controlId="new-product-description">
            <p>Description:</p>
            <textarea id="desc-textarea" className="form-control" rows="3" value={this.state.description} onChange={this.writingDesc.bind(this)} />
          </FormGroup>
          <p id="new-product-paragraph">{this.state.descLength}/150</p>
          {descWarning}
          <hr />
          <Button onClick={this.cancel.bind(this)}>Cancel</Button>&emsp;
          <Button onClick={this.nextStep.bind(this)} disabled={nextState}>Next</Button>
        </Col>
      </div>
    )
  }
}
