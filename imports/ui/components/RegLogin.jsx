//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { Router, Link,  browserHistory } from 'react-router';

//UI RELATED IMPORTS
  //BOOTSTRAP
  import Button from 'react-bootstrap/lib/Button';
  import Checkbox from 'react-bootstrap/lib/Checkbox';
  import Col from 'react-bootstrap/lib/Col';
  import Form from 'react-bootstrap/lib/Form';
  import FormControl from 'react-bootstrap/lib/FormControl';
  import FormGroup from 'react-bootstrap/lib/FormGroup';

//METHODS
import { registerNewUser } from '../../api/methods.jsx';

export default class RegLogin extends TrackerReact(Component) {

  constructor(props) {
    super(props);

    this.state = {
      selectVendor: false,
      isLoading: false,
    }
  }

  componentWillMount() {
    if (Meteor.userId()) {
      browserHistory.push('/');
    }
  }

  hideMobileKeybaord() {
    document.activeElement.blur();
    var inputs = document.querySelectorAll('input');
    for (var i = 0; i < inputs.length; i++) {
      inputs[i].blur();
    }
  };

  login(event) {
    event.preventDefault();
    if (event.keyCode == 13 || event.type == 'submit') {
      this.hideMobileKeybaord();
      var username = ReactDOM.findDOMNode(loginUsername).value.trim();
      var password = ReactDOM.findDOMNode(loginPassword).value.trim();
      //var currentRoute = FlowRouter.getRouteName();
      var currentRoute = location.pathname;
      //var productIdParam = FlowRouter.getParam('product_id');
      Meteor.loginWithPassword(username, password, function(error) {
        ReactDOM.findDOMNode(loginUsername).value = "";
        ReactDOM.findDOMNode(loginPassword).value = "";
        if (error) {
          Bert.alert('Incorrect username or password', 'danger');
          console.log(error);
        } else {
          //ReactDOM.findDOMNode(loginUsername).value = "";
          //ReactDOM.findDOMNode(loginPassword).value = "";
          if (currentRoute == '/login') {
            browserHistory.push('/');
          } else {
            browserHistory.push(currentRoute);
          }
        }
      });
    }
    /*
    if (currentRoute != "edit-product") {
      FlowRouter.go(currentRoute);
    } else {
      FlowRouter.go("/edit-product/"+productIdParam)
    }
    */
  }

  toggleVendorSelect(event) {
    this.setState({
      selectVendor: event.target.checked
    })
  }

  register(event) {
    event.preventDefault();
    this.setState({
      isLoading: true,
    })
    this.hideMobileKeybaord();
    var username = ReactDOM.findDOMNode(registerUsername).value.trim();
    var email = ReactDOM.findDOMNode(registerEmail).value.trim();
    var password = ReactDOM.findDOMNode(registerPassword).value.trim();
    var accountType = this.state.selectVendor;

    registerNewUser.call({
      username: username,
      email: email,
      password: password,
      account: accountType
    }, (error, result) => {
      this.setState({
        isLoading: false,
      })
      if (error) {
        console.log(error);
        Bert.alert(error.reason, 'danger');
      } else {
        Meteor.loginWithPassword(username, password);
        browserHistory.push('/');
      }
    });

    ReactDOM.findDOMNode(registerUsername).value = "";
    ReactDOM.findDOMNode(registerEmail).value = "";
    ReactDOM.findDOMNode(registerPassword).value = "";
  }

  render() {

    var checked = this.state.selectVendor? "checked" : "";

    let spinner = this.state.isLoading? <div className='spinner-container'><div className='spinner'></div></div> : '';

    return (
      <div>
        {spinner}
        <h3>Login</h3>
        <Form horizontal onSubmit={this.login.bind(this)}>

          <FormGroup controlId="loginUsername">
            <Col xs={8} sm={6} md={4} xsOffset={2} smOffset={3} mdOffset={4}>
              <FormControl type="text" placeholder="Username" />
            </Col>
          </FormGroup>

          <FormGroup controlId="loginPassword">
            <Col xs={8} sm={6} md={4} xsOffset={2} smOffset={3} mdOffset={4}>
              <FormControl type="password" onKeyUp={this.login.bind(this)} placeholder="Password" />
            </Col>
          </FormGroup>

          <FormGroup>
            <Col xs={8} sm={6} md={4} lg={2} xsOffset={2} smOffset={3} mdOffset={4} lgOffset={5}>
              <Button type="submit">Login</Button>
              {/*<Link to='/'>Login</Link>*/}
            </Col>
          </FormGroup>
          <Link to='/forgot-login'>Forgot username or password</Link>
        </Form>

        <h3>Register</h3>
        <Form horizontal onSubmit={this.register.bind(this)}>

          <FormGroup controlId="registerEmail">
            <Col xs={8} sm={6} md={4} xsOffset={2} smOffset={3} mdOffset={4}>
              <FormControl type="email" placeholder="Email" />
            </Col>
          </FormGroup>

          <FormGroup controlId="registerUsername">
            <Col xs={8} sm={6} md={4} xsOffset={2} smOffset={3} mdOffset={4}>
              <FormControl type="text" placeholder="Username" />
            </Col>
          </FormGroup>

          <FormGroup controlId="registerPassword">
            <Col xs={8} sm={6} md={4} xsOffset={2} smOffset={3} mdOffset={4}>
              <FormControl type="password" placeholder="Password" />
            </Col>
          </FormGroup>

          <FormGroup>
            <Col xs={8} sm={6} md={4} lg={2} xsOffset={2} smOffset={3} mdOffset={4} lgOffset={5}>
              <Checkbox onChange={this.toggleVendorSelect.bind(this)}>I want to be a vendor</Checkbox>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col xs={8} sm={6} md={4} lg={2} xsOffset={2} smOffset={3} mdOffset={4} lgOffset={5}>
              <Button type="submit">Register</Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    )
  }
}
