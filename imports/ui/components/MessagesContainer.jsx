//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import classNames from 'classnames';

//UI RELATED IMPORTS
  //Bootstrap
  import Button from 'react-bootstrap/lib/Button';
  import Col from 'react-bootstrap/lib/Col';
  import Form from 'react-bootstrap/lib/Form';
  import FormControl from 'react-bootstrap/lib/FormControl';
  import FormGroup from 'react-bootstrap/lib/FormGroup';
  import InputGroup from 'react-bootstrap/lib/InputGroup';
  import Jumbotron from 'react-bootstrap/lib/Jumbotron';

  //Components
  import Thread from './Thread.jsx';
  import MessagePane from './MessagePane.jsx';
  import ComposeMsgModal from './ComposeMsgModal.jsx';

//METHODS
import {sendMessage} from '../../api/methods.jsx';

//COLLECTIONS
//import { Threads } from '../../api/collections/threads.js';
//import { Messages } from '../../api/collections/messages.js';

export default class MessagesContainer extends TrackerReact(Component) {

  constructor() {
    super()

    this.transitionPanels = this.transitionPanels.bind(this);
    this.resetPanels = this.resetPanels.bind(this);

    this.state = {
      message: '',
      showModal: false,
      messagesCol: false,
    }
  }

  componentWillUnmount() {
    Session.set('threadId', null);
  }

  close() {
      this.setState({
        showModal: false,
      })
  }

  open() {
    this.setState({
      showModal: true,
    })
  }

  detectKey(event) {
    if (event.keyCode == 13) {
      this.messageSend(event);
    }
  }

  hideMobileKeyboard() {
    document.activeElement.blur();
    var inputs = document.querySelectorAll('input');
    for (var i = 0; i < inputs.length; i++) {
      inputs[i].blur();
    }
  }

  getToUser(threadId) {
    let thread = Threads.findOne({_id: threadId});
    if (thread.user1 == Meteor.userId()) {
      return thread.user2;
    } else {
      return thread.user1;
    }
  }

  messageSend(event) {
    event.preventDefault();
    let threadId = Session.get('threadId');
    if (!threadId) {
      Bert.alert('Error sending message, please try again', 'danger');
      throw new Meteor.Error('no-thread', 'No thread selected');
    }
    let toUser = this.getToUser(threadId);
    sendMessage.call({
      thread: Session.get('threadId'),
      to: toUser,
      message: this.state.message,
    }, (error) => {
      if (error) {
        Bert.alert(error, 'danger')
      } else {
        this.setState({
          message: ''
        })
      }
    })
  }

  resetPanels() {
    this.setState({
      messagesCol: '',
    })
  }

  transitionPanels() {
    this.setState({
      messagesCol: true,
    })
  }

  writingMsg(event) {
    this.setState({
      message: event.target.value
    })
  }

  render() {
    {/*
    let threadSub = Meteor.subscribe('threads');
    let msgSub = Meteor.subscribe('messages');

    if (!threadSub.ready() || !msgSub.ready()) {
      return (<div>Loading...</div>)
    }

    let threads = Threads.find({}, {sort: {timestamp: -1}}).fetch();

    let threadMap = threads.map(thread => {
      let messagesInThread = Messages.find({thread: thread._id}).fetch().length;
      if (thread.user1 == Meteor.userId() || messagesInThread > 0) {
        return <Thread key={thread._id} thread={thread} transition={this.transitionPanels} />
      }
    })

    let selectedThread = Session.get('threadId')? Session.get('threadId'): null;

    let messageBox = Session.get('threadId')? <Form inline id='msg-container' onSubmit={this.messageSend.bind(this)}>
                                                <FormGroup controlId='sendMessage'>
                                                  <FormControl componentClass='textarea' value={this.state.message} onChange={this.writingMsg.bind(this)} onKeyDown={this.detectKey.bind(this)} />
                                                </FormGroup>
                                                <Button bsSize='xsmall' type='submit' onClick={this.messageSend.bind(this)} disabled={sendDisable}>Send</Button>
                                              </Form> : '';

    let sendDisable = (this.state.message.length < 1)? true : false;

    let messageClasses = classNames({'messages-col-transition' : true, 'messages-col-show' : this.state.messagesCol});
    let threadClasses = classNames({'hideThreads' : this.state.messagesCol, 'threads-col-transition' : true});
    */}
    return (
      {/*<div id='messages-container'>
        <ComposeMsgModal id='composeMsgModal' show={this.state.showModal} onHide={this.close.bind(this)} />
        <Col className={threadClasses} sm={4} smOffset={1}>
          <Jumbotron>
            <div className='compose-btn' onClick={this.open.bind(this)}>⊕</div>
            {threadMap}
          </Jumbotron>
        </Col>
        <Col id='messagesCol' className={messageClasses} sm={6}>
          {Session.get('threadId')?
          <Jumbotron id='msgJumbo'>
            <div className='compose-btn' onClick={this.resetPanels}>↩</div>
            <MessagePane thread={selectedThread}/>
            <hr />
            {messageBox}
          </Jumbotron> : '' }
        </Col>
      </div>*/}
    )
  }
}
