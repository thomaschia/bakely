//BASIC IMPORTS//
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { browserHistory } from 'react-router';

//UI RELATED IMPORTS//
  //Bootstrap
  import Button from 'react-bootstrap/lib/Button';
  import Col from 'react-bootstrap/lib/Col';
  import ReactCrop from 'react-image-crop';

  //Components

  //Methods
  import { saveImage } from '../../api/methods.jsx';
  import { deleteImages } from '../../api/methods.jsx';

//COLLECTION IMPORTS//
import { Products } from '../../api/collections/products.js';

export default class ImageCropper extends TrackerReact(Component) {

  constructor(props) {
    super(props);

    this.state = {
      downloadLink: this.props.image || '',
      imgData: '',
      fileName: "product-image.jpg",
      newImg: null,
      isLoading: false,
      x: 10,
      y: 10,
      width: 80,
      subscription: {
        products: Meteor.subscribe('products'),
        bakeshop: Meteor.subscribe('bakeshop')
      }
    }
  }

  componentDidMount() {

    window.addEventListener("dragover", (e) => {
      e = e || event;
      if (e.target.id != 'dropzone') {
        e.preventDefault();
      }
    }, false);

    window.addEventListener("drop", (e) => {
      e = e || event;
      if (e.target.id != 'dropzone') {
        e.preventDefault();
      }
    }, false);

    if (this.state.downloadLink) {
      this.toBase64(this.state.downloadLink, (data) => {
        this.updateState({imgData: data});
      })
    }

  }

  componentWillUnmount() {
    this.state.subscription.products.stop();
    this.state.subscription.bakeshop.stop();
  }

  cancel() {
    if (Session.get('imageArray')) {
      //set up call to delete everything in the array
      let array = Session.get('imageArray').slice();
      deleteImages.callPromise({
        array: array,
      })
      .then(() => {
        Session.set('imageArray', null);
        browserHistory.push(this.getNextPath());
      })
      .catch((error) => {
        console.log(error);
      })
    } else {
      browserHistory.push(this.getNextPath());
      //browserHistory.push(`/edit-product/${this.props._id}`);
      //FlowRouter.go('/edit-product/' + this.props.product._id);
    }
  }

getNextPath() {

  let pathComponent;

  switch(this.props.type) {
    case 'edit-product':
      pathComponent = `/edit-product/${this.props.id}`;
      break;

    case 'new-product':
      pathComponent = `/new-product/${this.props.id}`;
      break;

    case 'shop':
    case 'cover':
      pathComponent = '/my-shop';
      break;

    default:
      pathComponent = '/my-shop';
  }

  return pathComponent;
}

/*
  handleSave() {
    this.setState({
      isLoading: true
    })

    let product = Products.findOne({_id: this.props.product._id});
    let oldImagePath = product.image;
    //
    let blob = this.saveImage();
    this.upload(blob, () => {
      if (Session.get('imageArray')) {
        let array = Session.get('imageArray');
        saveNewProductImage.call({
          productId: this.props.product._id,
          urlToDelete: oldImagePath,
          array: array
        }, (error) => {
          if (error) {
            console.log(error);
          } else {
            this.setState({
              isLoading: false
            })
            Bert.alert("Success", 'success');
            Session.set('imageArray', null);
            browserHistory.push(`/edit-product/${this.props.product._id}`);
            //FlowRouter.go('/edit-product/' + this.props.product._id);
          }
        });
      }
    });
  }
*/
  handleSavePromise() {
    let product = Products.findOne({_id: this.props.id});
    let oldImagePath = this.props.image;
    let blob = this.saveImage();
    this.uploadPromise(blob)
      .then(this.buildArrayForDelete)
      .then(response => {
        if (Session.get('imageArray')) {
          let array = Session.get('imageArray');
          saveImage.callPromise({
            updateType: this.props.type,
            id: this.props.id,
            urlToDelete: oldImagePath,
            array: array
          })
          .then(() => {
            Bert.alert('Success', 'success');
            Session.set('imageArray', null);
            browserHistory.push('/my-shop');
            //browserHistory.push(`/edit-product/${this.props.product._id}`)
          })
          .catch((error) => {
            console.log(error);
          })
        }
      })
      .catch(error => {
        console.log(error);
      })
  }

  saveImage() {
    let createdBlob = this.createBlob(this.state.newImg);
    return createdBlob;
  }

  createBlob(dataURL) {
    var byteString = atob(dataURL.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURL.split(',')[0].split(':')[1].split(';')[0]

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    return new Blob([ab], {type: mimeString});
  }

  uploadPromise(droppedFile) {
    let fileName = this.state.fileName;
    return new Promise((resolve, reject) => {
      let imgName = {imageName: fileName};
      let uploader = new Slingshot.Upload('imageUploads', imgName);
      uploader.send(droppedFile, (error, result) => {
        if (error) {
          reject(error);
        } else {
          this.setState({
            downloadLink: result
          });
          resolve(result);
        }
      })
    });
  }

  buildArrayForDelete(result) {
    return new Promise((resolve, reject) => {
      if (Session.get('imageArray')) {
        let array = Session.get('imageArray').slice(0);
        array.push(result);
        Session.set('imageArray', array);
        resolve(result);
      } else {
        Session.set('imageArray', [result]);
        resolve(result);
      }
    });
  }

  upload(droppedFile, callback) {
    this.setState({
      isLoading: true
    })
    let imgName = {imageName: this.state.fileName};

    var uploader = new Slingshot.Upload("imageUploads", imgName);
    //COMMENT OUT THE UPLOAD FOR DEV PURPOSES
    uploader.send(droppedFile, (error, downloadUrl) => {

      if (error) {
        console.error('Error uploading', uploader.hxr.response);
        alert(error);
      } else {
        this.setState({
          downloadLink: downloadUrl,
          isLoading: false
        });
        if (Session.get('imageArray')) {
          let array = Session.get('imageArray').slice(0);
          array.push(downloadUrl);
          Session.set('imageArray', array);
        } else {
          Session.set('imageArray', [downloadUrl]);
        }
        if (callback) {
          callback();
        }
      }
    });
    //End Comment Out
  }

  onDrop(event) {
    let imgName = event.target.files[0].name;
    let fileToUpload = event.target.files[0];
    this.setState({
      fileName: imgName
    }, () => {
      this.upload(fileToUpload, () => {
        this.toBase64(this.state.downloadLink, (data) => {
          this.updateState({imgData: data});
        })
      });
    });
  }

  loadImage(callback) {
    var image = new Image();
    image.crossOrigin = "anonymous";
    image.onload = () => {
      callback(image);
      image = null;
    };
    //Append dynamic query to force reload of images (CORS caching error)
    image.src = this.state.downloadLink + '?' + new Date().getTime(); //Here be changes
  }

  cropImage(crop) {

    this.setState({
      x:  crop.x,
      y: crop.y,
      width: crop.width
    });

    this.loadImage(cropAfterLoad.bind(this));

    function cropAfterLoad(loadedImg) {
      var imageWidth = loadedImg.width;
      var imageHeight = loadedImg.height;

      var cropX = (crop.x / 100) * imageWidth;
      var cropY = (crop.y / 100) * imageHeight;

      var cropWidth = (crop.width / 100) * imageWidth;
      var cropHeight = (crop.height / 100) * imageHeight;

      var canvas = document.createElement('canvas');
      canvas.width = cropWidth;
      canvas.height = cropHeight;
      var ctx = canvas.getContext('2d');
      ctx.drawImage(loadedImg, cropX, cropY, cropWidth, cropHeight, 0, 0, cropWidth, cropHeight);
      this.setState({
        newImg: canvas.toDataURL('image/jpeg')
      });
    }
  }

  toBase64(src, callback) {

    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function() {

      var canvas = document.createElement('CANVAS');
      var ctx = canvas.getContext('2d');
      var dataURL;
      canvas.height = this.height;
      canvas.width = this.width;
      ctx.drawImage(this, 0, 0);
      dataURL = canvas.toDataURL('image/jpeg');
      callback(dataURL);
    };
    img.src = src+'?'+ new Date().getTime();
    if (img.complete || img.complete === undefined) {
      img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
      img.src = src;
    }
  }

  updateState(state) {
    this.setState(state);
  }

  render() {

    let aspect = 1/1;

    if (this.props.type == 'cover') {
      aspect = 3/1;
    }

    var cropSettings = {
      aspect: aspect,
      x: this.state.x,
      y: this.state.y,
      width: this.state.width
    };

    if (this.state.imgData) {
      var reactCrop = <ReactCrop src={this.state.imgData} crop={cropSettings} onComplete={this.cropImage.bind(this)} />;
    }

    let spinner = this.state.isLoading? <div className='spinner-container'><div className='spinner'></div></div> : '';

    return (
      <Col xs={10} xsOffset={1}>
        {spinner}
        <div className="upload-area">
          <form>
            <p className="alert">
              <span>Click or Drag a File Here to Upload</span>
              <input id="dropzone" onChange={this.onDrop.bind(this)} type="file" />
            </p>
          </form>
        </div>
        <Col className="crop-container-col" sm={6}>
          <p>Original Image:</p>
          <div className="crop-container">
            {reactCrop}
          </div>
        </Col>
        <Col id='preview-div' sm={6}>
          <p>Preview:</p>
          <img className="cropped-image" src={this.state.newImg} />
        </Col>
        <Col className="crop-save-button">
          <Button onClick={this.handleSavePromise.bind(this)}>Save</Button>&emsp;
          <Button onClick={this.cancel.bind(this)}>Cancel</Button>
        </Col>
      </Col>
    )
  }
}
