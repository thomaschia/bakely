import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { handleVerify } from '../../api/methods.jsx';
import { handleEnable } from '../../api/methods.jsx';

export default class UserPanel extends Component {

  handleVerify() {
    handleVerify.call({
      userId: this.props.data.userId
    }, (error, result) => {
      if (error) {
        console.log(error.message)
      } else {
        this.props.getUsers()
      }
    })
  }

  handleEnable() {
    handleEnable.call({
      shopId: this.props.data.shopId
    }, (error, result) => {
      if (error) {
        console.log(error.message);
      }
    })
  }

  render() {

    return (
      <div className='user-panel-container'>
        <div className='user-panel-username-container'>
          <div className='user-panel-username-label'>
            <p>Username: {`${this.props.data.username}`}</p>
          </div>
        </div>
        <div className='user-panel-verify-container'>
          <div className='user-panel-verify-label'>
            <p>Verified: {this.props.data.verified ? 'Yes' : 'No'}</p>
          </div>
          {this.props.data.verified ?
          <div onClick={this.handleVerify.bind(this)} className='user-panel-revoke-button'>
            <p>Revoke</p>
          </div> :
          <div onClick={this.handleVerify.bind(this)} className='user-panel-verify-button'>
            <p>Verify</p>
          </div>
          }
        </div>
        <div className='user-panel-shop-container'>
          <div className='user-panel-shop-label'>
            <p>
              Shop: {this.props.data.shop ?
                this.props.data.visible? 'Visible' : 'Invisible'
                : 'None'}
            </p>
          </div>
          {this.props.data.shop ?
            this.props.data.visible ?
              <div onClick={this.handleEnable.bind(this)} className='user-panel-disable-button'><p>Disable</p></div> :
              <div onClick={this.handleEnable.bind(this)} className='user-panel-enable-button'><p>Enable</p></div> :
            ''
          }
        </div>
      </div>
    )
  }
}
