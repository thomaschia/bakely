//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { IndexLink, Link, browserHistory } from 'react-router';
import classNames from 'classnames';

//UI RELATED IMPORTS
  //BOOTSTRAP
  import Badge from 'react-bootstrap/lib/Badge';
  import Nav from 'react-bootstrap/lib/Nav';
  import Navbar from 'react-bootstrap/lib/Navbar';
  import NavItem from 'react-bootstrap/lib/NavItem';

  //COMPONENTS

//COLLECTION IMPORTS
//import { Messages } from '../../api/collections/messages.js';
//import { Threads } from '../../api/collections/threads.js';

export default class NaviBar extends TrackerReact(Component) {

  constructor(props) {
    super(props)

    this.toggleMenu = this.toggleMenu.bind(this);

    this.state = {
      menuHidden: this.props.menuHidden
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.menuHidden != this.state.menuHidden) {
      this.setState({
        menuHidden: nextProps.menuHidden
      })
    }
  }

  goToLink(event) {
    event.preventDefault();
    Meteor.logout();
  }

  goHome() {
    //FlowRouter.go('home');
  }

  toggleMenu() {
    let menuToggled = this.state.menuHidden;
    this.setState({
      menuHidden: !menuToggled
    })
  }

  render() {

    let toggleMenu = classNames({
      'displayNone': !this.state.menuHidden
    });

    let accountUL = classNames({
      'account-ul': true,
      'displayNone': !this.state.menuHidden
    })

    let authLink = Meteor.userId()? "Logout" : "Login";

    let selectedNav;

    return (
      <div className='navibar-container'>
        <nav className='main-categories'>
          <ul>
            <ul className={toggleMenu}>
              <li>
                <IndexLink to='/'>Home</IndexLink>
              </li>
              <li>
                <Link to='/all-shops'>Shops</Link>
              </li>
              <li>
                <Link to='/about-us'>About Us</Link>
              </li>
            </ul>
            <ul className={accountUL}>
              <li></li>
              <li>
                {Meteor.userId()? <Link to='/my-account'>My Account</Link> : ''}
              </li>
              <li>
                {Meteor.userId()? <Link to='' onClick={this.goToLink.bind(this)}>Logout</Link> : <Link to='/login'>Login</Link>}
              </li>
            </ul>
          </ul>

          {/*
          <ul>
            <ul>
              <li onClick={this.toggleMenu} className='toggle-main-menu'>☰</li>
            </ul>
            <ul className={toggleMenu}>
              <li>
                <IndexLink to='/'>Home</IndexLink>
              </li>
              <li>
                <Link to='/all-shops'>Shops</Link>
              </li>
              <li>
                <Link to='/about-us'>About Us</Link>
              </li>
            </ul>
            <ul className={toggleMenu}>
              <li>
                {Meteor.userId()? <Link to='/my-account'>My Account</Link> : ''}
              </li>
              <li>
                {Meteor.userId()? <Link to='' onClick={this.goToLink.bind(this)}>Logout</Link> : <Link to='/login'>Login</Link>}
              </li>
            </ul>
          </ul>
          */}
        </nav>
      </div>
    )
  }
}
