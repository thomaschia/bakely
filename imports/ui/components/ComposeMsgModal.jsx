//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //Bootstrap
  import Button from 'react-bootstrap/lib/Button';
  import Col from 'react-bootstrap/lib/Col';
  import FormControl from 'react-bootstrap/lib/FormControl';
  import FormGroup from 'react-bootstrap/lib/FormGroup';
  import Glyphicon from 'react-bootstrap/lib/Glyphicon';
  import InputGroup from 'react-bootstrap/lib/InputGroup';
  import Modal from 'react-bootstrap/lib/Modal';

  //Components
  import SearchResults from './SearchResults.jsx';

//METHODS
import {createThread} from '../../api/methods.jsx';

//COLLECTIONS
import { Bakeshop } from '../../api/collections/bakeshop.js';

export default class ComposeMsgModal extends TrackerReact(Component) {

  constructor(props) {
    super(props)

    this.compose = this.compose.bind(this);

    this.state = {
      searchQuery: '',
      infNumber: 10,
    }
  }

  compose() {
    if (Session.get('selectedShop')) {
      let shopId = Session.get('selectedShop');
      let shop = Bakeshop.findOne({_id: shopId});
      let shopOwner = shop.owner;
      createThread.call({
        id: shopOwner
      }, (error, data) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
          this.props.onHide();
        } else {
          this.props.onHide();
          Session.set('threadId', data);
          //do something
        }
      });
    }
  }

  handleScroll(event) {
    let modal = event.target;
    if (modal.scrollTop - 20 === (modal.scrollHeight - modal.offsetHeight)) {
      let showing = this.state.infNumber;
      this.setState({
        infNumber: showing + 5
      })
    }
  }

  searching(event) {
    event.preventDefault();
    this.setState({
      searchQuery: event.target.value
    })
  }

  render() {

    let self = this;

    return (
      <Modal show={this.props.show} onHide={this.props.onHide} bsSize="sm" aria-labelledby="contained-modal-title-sm" dialogClassName="searchModal">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-sm">
            <Button onClick={this.compose}>Send</Button>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body id="search-modal">
          <FormGroup controlId="searchInput">
            <InputGroup>
              <FormControl type="text" value={this.state.searchQuery} onChange={this.searching.bind(this)} />
              <InputGroup.Button>
                <Button><Glyphicon glyph="search" /></Button>
              </InputGroup.Button>
            </InputGroup>
          </FormGroup>
        </Modal.Body>
        <Modal.Footer id='searchModalFooter' onScroll={this.handleScroll.bind(this)}>
          <SearchResults query={this.state.searchQuery} limit={this.state.infNumber} />
        </Modal.Footer>
      </Modal>
    )
  }
}
