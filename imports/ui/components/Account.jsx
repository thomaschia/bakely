//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //BOOTSTRAP
  import Button from 'react-bootstrap/lib/Button';
  import Label from 'react-bootstrap/lib/Label';

  //COMPONENTS
  import EditAccount from './EditAccount.jsx';
  import RegLogin from './RegLogin';
  import VendorAccountMain from './VendorAccountMain.jsx';

  //METHODS
  import { sendMail } from '../../api/methods.jsx';

export default class Account extends TrackerReact(Component) {

  constructor(props) {
    super(props)

    this.logout = this.logout.bind(this);
  }

  logout() {
    Meteor.logout();
  }

  render() {

    let currentUser = Meteor.userId();

    if (!currentUser) {
      return (<RegLogin />)
    }

    if (!Meteor.user()) {
      return (<div>Loading</div>)
    }

    let isVendor = Roles.userIsInRole(Meteor.userId(), 'vendor')? <VendorAccountMain /> : "";

    return (
      <div>
        <h4>Welcome {Meteor.user().username}</h4><br />
        {isVendor}
        <EditAccount />
        <div className='account-logout'>
          <p onClick={this.logout}>Logout</p>
        </div>
      </div>
    )
  }
}
