//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //Bootstrap

  //Components
  import Product from './Product.jsx';
  import SearchPanel from './SearchPanel.jsx';

//METHODS

//COLLECTION IMPORTS
import { Products } from '../../api/collections/products.js';

export default class SearchResults extends TrackerReact(Component) {

  constructor() {
    super()

    this.bound_scrollBottom = this.handleScroll.bind(this);
    this.sortBy = this.sortBy.bind(this);
    this.readyIn = this.readyIn.bind(this);

    this.state = {
      limitNumber: 10,
      sortBy: 'A - Z',
      readyIn: 'All'
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.bound_scrollBottom);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.bound_scrollBottom);
  }

  handleScroll(event) {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      let showing = this.state.limitNumber;
      this.setState({
        limitNumber: showing + 5
      })
    }
  }

  sortBy(sort) {
    this.setState({
      sortBy: sort
    })
  }

  readyIn(ready) {
    this.setState({
      readyIn: ready
    })
  }

  render() {

    const sortBy = {
      title: 'Sort by',
      buttonOne: 'A - Z',
      buttonTwo: 'Z - A',
      buttonThree: '$ High - Low',
      buttonFour: '$ Low - High'
    };

    const readyIn = {
      title: 'Ready In',
      buttonOne: 'All',
      buttonTwo: '0 - 24h',
      buttonThree: '1 - 3 days',
      buttonFour: 'More than 3 days'
    };

    Tracker.autorun(() => {
      Meteor.subscribe('products', this.props.params.query, this.state.limitNumber, this.state.sortBy, this.state.readyIn);
    })

    let query = {};
    let projection = {};

    switch(this.state.sortBy) {
      case 'A - Z':
      projection.sort = {name: 1}
      break;
      case 'Z - A':
      projection.sort = {name: -1}
      break;
      case '$ High - Low':
      projection.sort = {price: -1}
      break;
      case '$ Low - High':
      projection.sort = {price: 1}
      break;
    }

    switch(this.state.readyIn) {
      case 'All':
      query = {visible: true};
      break;
      case '0 - 24h':
      query = {visible: true, readyIn: '0 - 24h'};
      break;
      case '1 - 3 days':
      query = {visible: true, readyIn: '1 - 3 days'};
      break;
      case 'More than 3 days':
      query = {visible: true, readyIn: 'More than 3 days'};
      break;
    }

    console.log(query);

    let products = Products.find(query, projection).fetch().map(product => {
      return <Product key={product._id} product={product} />
    })

    return (
      <div className='search-results-main'>
        <div className='search-results-header-text'>
          Search results for {`'${this.props.params.query}'`}
        </div>
        <div className='search-results-container'>
          <div className='search-results-side-panel'>
            <SearchPanel options={sortBy} update={this.sortBy} />
            <SearchPanel options={readyIn} update={this.readyIn} />
          </div>
          <div className='search-results-products'>
            {products}
          </div>
      </div>
      </div>
    )
  }
}
