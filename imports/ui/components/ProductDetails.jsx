//BASIC IMPORTS//
import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { Link } from 'react-router';

//UI RELATED IMPORTS
  //BOOTSTRAP IMPORTS
  import Col from 'react-bootstrap/lib/Col';
  import Image from 'react-bootstrap/lib/Image';

//COLLECTION IMPORTS
import { Bakeshop } from '../../api/collections/bakeshop.js';

export default class ProductDetails extends TrackerReact(Component) {

  render() {

    return (
      <div className='product-details-container'>
        <div className='product-details-img'>
          <img src={this.props.product.image} alt='product_image' />
        </div>
        <div className='product-details-info'>
          <div className='product-details-info-name'>
            <p>{this.props.product.name}</p>
          </div>
          <div className='product-details-info-price'>
            <p>${this.props.product.price}</p>
          </div>
          <div className='product-details-info-shop'>
            <p>{this.props.shop.name}</p>
          </div>
          <div className='product-details-info-time'>
            <p>↻&nbsp;24h</p>
          </div>
          <div className='product-details-info-description'>
            <p>{this.props.product.description}</p>
          </div>
          <div className='product-details-info-contact'>
            {Meteor.userId() ? <Link to={`/contact/${this.props.shop._id}`}>Contact {this.props.shop.name}</Link> : <Link to='/login'>Log in to send a message</Link>}
          </div>
        </div>
      </div>
    )
/*
    return (
      <div className="product-details">
      <Col xs={10} xsOffset={1} md={3}>
          <Image src={this.props.product.image} rounded />
          <hr />
        </Col>
        <Col xs={10} xsOffset={1} md={6}>
          <h2>{this.props.product.name}</h2>
          <br />
          <h4>${this.props.product.price}</h4>
          <br />
          <p>{this.props.product.description}</p>
          <hr />
        </Col >
        <Col xs={10} xsOffset={1} md={6}>
          <p>Contact {this.props.shop.name} to purchase</p>
        </Col>
      </div>
    )
*/
  }
}
