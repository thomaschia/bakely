import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class StarRating extends TrackerReact(Component) {

  constructor(props) {

    super(props)

    this.state = {
      date: this.props.date
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.date != this.state.date) {
      this.setState({
        date: nextProps.date
      })
    }
  }

  render() {

    const rating = 4;

    let goldStar = <img src='/images/gold_star.png' alt='gold star' />
    let greyStar = <img src='/images/grey_star.png' alt='grey star' />

    let stars = [];
    for (let i = 0; i < rating; i++ ) {
      stars[i] = <img key={i} src='/images/gold_star.png' alt='gold star' />;
    }
    if (rating < 5) {
      for (let i = 5; i > rating; i-- ) {
          stars[i] = <img key={i} src='/images/grey_star.png' alt='grey star' />
      }
    }

    if (this.state.date) {
      date = this.state.date;
      month = date.getMonth();
      year = date.getFullYear().toString().substr(2,2);
    } else {
      month = 0;
      year = 2016;
    }

    switch (month) {
      case 0:
        month = 'Jan';
        break;
      case 1:
        month = 'Feb';
        break;
      case 2:
        month = 'Mar';
        break;
      case 3:
        month = 'Apr';
        break;
      case 4:
        month = 'May';
        break;
      case 5:
        month = 'Jun';
        break;
      case 6:
        month = 'Jul';
        break;
      case 7:
        month = 'Aug';
        break;
      case 8:
        month = 'Sept';
        break;
      case 9:
        month = 'Oct';
        break;
      case 10:
        month = 'Nov';
        break;
      case 11:
        month = 'Dec';
        break;
      default:
        month = 'Jan';
    }

    let dateJoined = `${month} '${year}`

    return (
      <div className='star-rating'>
        <span className='join-date'>Joined: {dateJoined}</span>
      </div>
    )
  }
}
