//BASIC IMPORTS//
import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //Components
  import NewProduct from './NewProduct.jsx';

//COLLECTION IMPORTS
import { Products } from '../../api/collections/products.js';


export default class NewProductContainer extends TrackerReact(Component) {

  constructor() {
    super()

    this.state = {
      subscription: {
        products: Meteor.subscribe('products')
      }
    }
  }

  componentWillUnmount() {
    this.state.subscription.products.stop();
  }

  render() {

    if (this.props.params.product_id == 'new') {
      let product = {
        name: '',
        price: '',
        readyIn: '0 - 24h',
        description: ''
      }
      return (<NewProduct product={product} />)
    } else {
      let product = Products.findOne({_id: this.props.params.product_id})
      return (<NewProduct product={product} />)
    }
  }
}
