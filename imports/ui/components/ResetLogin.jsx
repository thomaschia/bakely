//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';

//UI RELATED IMPORTS
  //Bootstrap
  import Button from 'react-bootstrap/lib/Button';
  import Col from 'react-bootstrap/lib/Col';
  import Form from 'react-bootstrap/lib/Form';
  import FormControl from 'react-bootstrap/lib/FormControl';
  import FormGroup from 'react-bootstrap/lib/FormGroup';

  //Components

//METHODS

//COLLECTIONS

export default class ResetLogin extends Component {

  handleSubmit(event) {
    event.preventDefault();
    let password = ReactDOM.findDOMNode(resetPassword).value.trim();
    let confirmPassword = ReactDOM.findDOMNode(confirmResetPassword).value.trim();
    if (password != confirmPassword) {
      Bert.alert('Passwords do not match', 'danger');
      throw new Meteor.Error('match-fail', 'Passwords do not match');
    }
    let token = this.props.params.token;
    Accounts.resetPassword(token, password, (error) => {
      if (error) {
        Bert.alert('Error resetting password, please try again', 'danger');
        browserHistory.push('/forgot-login');
        //FlowRouter.go('forgot-login');
      } else {
        Bert.alert('Success', 'success');
        browserHistory.push('/login');
        //FlowRouter.go('login');
      }
    })
  }

  render() {

    return (
      <div>
        <Form horizontal onSubmit={this.handleSubmit.bind(this)}>
          <Col xs={8} sm={6} md={4} lg={2} xsOffset={2} smOffset={3} mdOffset={4} lgOffset={5}>
            <p>Choose a new password below</p>
            <hr />
          </Col>
          <FormGroup controlId="resetPassword">
            <Col xs={8} sm={6} md={4} lg={2} xsOffset={2} smOffset={3} mdOffset={4} lgOffset={5}>
              <FormControl type="password" placeholder="Password" />
            </Col>
          </FormGroup>
          <FormGroup controlId="confirmResetPassword">
            <Col xs={8} sm={6} md={4} lg={2} xsOffset={2} smOffset={3} mdOffset={4} lgOffset={5}>
              <FormControl type="password" placeholder="Confirm Password" />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col xs={8} sm={6} md={4} lg={2} xsOffset={2} smOffset={3} mdOffset={4} lgOffset={5}>
              <Button type="submit">Reset</Button>
            </Col>
          </FormGroup>
      </Form>
      </div>
    )
  }
}
