//BASIC IMPORTS//
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { browserHistory, Link } from 'react-router';

//UI RELATED IMPORTS//
  //BOOTSTRAP
  import Alert from 'react-bootstrap/lib/Alert';
  import Button from 'react-bootstrap/lib/Button';
  import Col from 'react-bootstrap/lib/Col';
  import FormControl from 'react-bootstrap/lib/FormControl';
  import FormGroup from 'react-bootstrap/lib/FormGroup';
  import Image from 'react-bootstrap/lib/Image';

  //COMPONENTS
  import ProductEdit from './ProductEdit.jsx';
  import AddProduct from './AddProduct.jsx';

//METHODS
import { saveDescription } from '../../api/methods.jsx';
import { saveName } from '../../api/methods.jsx';
import { getMemberTier } from '../../api/methods.jsx';

//COLLECTION IMPORTS//
import { Products } from '../../api/collections/products.js';

export default class MyShop extends TrackerReact(Component) {

  constructor(props) {
    super(props)

    this.state = {
      descLength: this.props.shop.description.length,
      shopDesc: this.props.shop.description,
      nameLength: this.props.shop.name.length,
      shopName: this.props.shop.name,
      productLimit: this.props.productLimit,
    }
  }

  balanceProducts(numberUsed) {
    var balanceArray = [];
    for (var i = 1; i <= this.state.productLimit - numberUsed; i++) {
      balanceArray.push(<AddProduct key={i}/>)
    }
    return balanceArray;
  }

  revertShopName(event) {
    event.preventDefault();
    this.setState({
      nameLength: this.props.shop.name.length,
      shopName: this.props.shop.name
    });
  }

  revertShopDesc(event) {
    event.preventDefault();
    this.setState({
      descLength: this.props.shop.description.length,
      shopDesc: this.props.shop.description
    });
  }

  editShopImg() {
    browserHistory.push(`/edit-shop-image/${this.props.shop._id}`);
    //FlowRouter.go('/edit-shop-image/' + this.props.shop._id);
  }

  coverPicCrop() {
    browserHistory.push(`/edit-cover-pic/${this.props.shop._id}`);
  }

  saveShopDesc(event) {
    event.preventDefault();
    var shopDesc = $('#desc-textarea').val().trim();
    if (this.state.descLength > 150) {
      Bert.alert('Exceeeded maximum allowed description length', 'danger');
      throw new Meteor.Error("exceeded-description-length", "You have exceeded the maximum allowed description length");
    }
    saveDescription.call({
      shopId: this.props.shop._id,
      shopDesc: shopDesc
    }, (error, result) => {
      if (error) {
        Bert.alert(error.reason, 'danger')
      } else {
        Bert.alert('Success', 'success')
      }
    })
  }

  saveShopName(event) {
    event.preventDefault();
    var shopName = ReactDOM.findDOMNode(editNameForm).value.trim();
    if (this.state.nameLength > 20) {
      Bert.alert('Exceeeded maximum allowed name length', 'danger');
      throw new Meteor.Error("exceeded-name-length", "You have exceeded the maximum allowed name length");
    }
    saveName.call({
      shopId: this.props.shop._id,
      shopName: shopName
    }, (error, result) => {
      if (error) {
        Bert.alert(error.reason, 'danger')
      } else {
        Bert.alert('Success', 'success')
      }
    })
  }

  writingDesc(event) {
    event.preventDefault();
    this.setState({
      descLength: event.target.value.trim().length,
      shopDesc: event.target.value
    })
  }

  writingName(event) {
    event.preventDefault();
    this.setState({
      nameLength: event.target.value.trim().length,
      shopName: event.target.value
    })
  }

  render() {

    Meteor.subscribe('products');

    let productsArray = Products.find({owner: Meteor.userId()}).fetch();
    //let shopImageURL = "/images/" + this.props.shop.shop_img;

    let productsMap = productsArray.map(product => {
      return <ProductEdit key={product._id} product={product} />
    });

    let balanceProducts = this.balanceProducts(productsArray.length);

    let exceedDescLength = (this.state.descLength > 150)? <Alert bsStyle='danger'>Exceeded max length</Alert> : "";
    let saveButtonState = (this.state.descLength > 150)? true : false;

    let exceedNameLength = (this.state.nameLength > 20)? <Alert bsStyle='danger'>Exceeded max length</Alert> : "";
    let nameSaveButtonState = (this.state.nameLength > 20)? true : false;

    return (
      <div className="my-shop-container">
        <div className='back-to-account'>
          <Link to={'/my-account'}>Back to My Account</Link>
        </div>
        <Col xs={10} xsOffset={1} md={3} mdOffset={1}>
          <Image src={this.props.shop.shop_img} rounded />
          <br />
          <Button className="edit-image-btn" onClick={this.editShopImg.bind(this)}>Edit Profile Pic</Button>
          <hr />
          {this.props.shop.cover_img ? <Button className="edit-image-btn" onClick={this.coverPicCrop.bind(this)}>Edit Cover Pic</Button> : <Button className="edit-image-btn" onClick={this.coverPicCrop.bind(this)}>Add Cover Pic</Button>}
          <hr />
        </Col>
        <Col xs={10} xsOffset={1} md={6} mdOffset={0}>
          <FormGroup controlId="editNameForm">
            <p>Name (20 characters max): </p><FormControl type="text" value={this.state.shopName} onChange={this.writingName.bind(this)} />
            <br />
            <p>{this.state.nameLength}/20</p>{exceedNameLength}
            <Button className="save-name-btn" onClick={this.saveShopName.bind(this)} disabled={nameSaveButtonState}>Save</Button>&emsp;
            <Button className="revert-name-btn" onClick={this.revertShopName.bind(this)}>Revert</Button>
          </FormGroup>
          <hr />
          <FormGroup controlId="editDescForm">
            <p>Description (150 characters max): </p><textarea id="desc-textarea" className="form-control" rows="3" value={this.state.shopDesc} onChange={this.writingDesc.bind(this)} />
            <br />
            <p>{this.state.descLength}/150</p>{exceedDescLength}
            <Button className="save-desc-btn" onClick={this.saveShopDesc.bind(this)} disabled={saveButtonState}>Save</Button>&emsp;
            <Button className="revert-desc-btn" onClick={this.revertShopDesc.bind(this)}>Revert</Button>
          </FormGroup>
          <hr />
          {productsMap}
          {balanceProducts}
        </Col>
      </div>
    )
  }
}
