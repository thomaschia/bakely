//BASIC IMPORTS
import React, { Component } from 'react';
import { browserHistory } from 'react-router';

//UI RELATED IMPORTS
  //BOOTSTRAP IMPORTS
  import Col from 'react-bootstrap/lib/Col';
  import Image from 'react-bootstrap/lib/Image';
  import Thumbnail from 'react-bootstrap/lib/Thumbnail';

export default class Product extends Component {

  goToDetails() {
    browserHistory.push(`/product-details/${this.props.product._id}`);
    //FlowRouter.go("/product-details/" + this.props.product._id);
  }

  render() {

    {/*return (
      <Col id="product-thumbnail" lg={3} md={4} sm={4} xs={6} lgOffset={0} mdOffset={0} smOffset={0} xsOffset={3}>
        <Thumbnail src={this.props.product.image} onClick={this.goToDetails.bind(this)}>
          <div className='product-name-text'>
            <span>{this.props.product.name}</span>
          </div>
          <div className='product-price-text'>
            <span>${this.props.product.price}</span>
          </div>
        </Thumbnail>
      </Col>
    )*/}

    return (
      <div id='product-thumbnail'>
        <Thumbnail src={this.props.product.image} onClick={this.goToDetails.bind(this)}>
          <div className='product-name-text'>
            <span>{this.props.product.name}</span>
          </div>
          <div className='product-price-text'>
            <span>${this.props.product.price}</span>
          </div>
        </Thumbnail>
      </div>
    )
  }
}
