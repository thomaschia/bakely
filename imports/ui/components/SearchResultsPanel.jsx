//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //Bootstrap
  import Panel from 'react-bootstrap/lib/Panel';

  //Components

//METHODS

//COLLECTION IMPORTS

export default class SearchResultsPanel extends TrackerReact(Component) {

  constructor() {
    super()

    this.selectPanel = this.selectPanel.bind(this);
  }

  componentWillUnmount() {
    Session.set('selectedShop', null);
  }

  selectPanel() {
    Session.set('selectedShop', this.props.shop._id);
  }

  render() {

    let selected = (Session.get('selectedShop') == this.props.shop._id)? 'info' : 'default';

    return (
      <Panel bsStyle={selected} header={this.props.shop.name} onClick={this.selectPanel}>Shop Location</Panel>
    )
  }
}
