//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';

//UI RELATED IMPORTS
  //Bootstrap
  import Col from 'react-bootstrap/lib/Col';
  import Glyphicon from 'react-bootstrap/lib/Glyphicon';
  import Image from 'react-bootstrap/lib/Image';

  //Components

//METHODS

//COLLECTION IMPORTS

export default class HomePage extends Component {

  render() {

    return (
      <div className='home-page-div'>
        <hr />
        <h3>Editor's Pick</h3>
        <div className='editors-pick'>
          {this.props.product ?
          <div className='editors-pick-img'>
            <img src={this.props.product.image} alt='product_image' />
          </div> : <p>Coming Soon</p> }
          {this.props.product ?
          <div className='editors-pick-text'>
            <h4>{this.props.product.name}</h4>
            <p>{this.props.product.description}</p>
            <Link to={`/product-details/${this.props.product._id}`}>See Product</Link>
          </div> : <p>Coming Soon</p> }
        </div>
        <hr />
        <h3>Featured Shop</h3>
        <div className='featured-shop'>
          {this.props.shop ?
          <div className='featured-shop-img'>
            <img src={this.props.shop.shop_img} alt='shop_image' />
          </div> : <p>Coming Soon</p> }
          {this.props.shop ?
          <div className='featured-shop-text'>
            <h4>{this.props.shop.name}</h4>
            <p>{this.props.shop.description}</p>
            <Link to={`/shops/${this.props.shop._id}`}>See Shop</Link>
          </div> : <p>Coming Soon</p> }
        </div>
        <hr />
        <h3>Sell With Us</h3>
        <div className='sell-with-us-container'>
          <div className='sell-item-container'>
            <div className='sell-item-img'>
              <img src='/images/register.png' alt='register_icon' />
            </div>
            Register as a vendor
          </div>
          <div className='sell-item-container'>
            <div className='sell-item-img'>
              <img src='/images/shop.png' alt='shop_icon' />
            </div>
            Once verified, create your shop
          </div>
          <div className='sell-item-container'>
            <div className='sell-item-img'>
              <img src='/images/sell.png' alt='sell_icon' />
            </div>
            Start listing your products!
          </div>
        </div>
        <hr />
        <div className='home-page-footer'>
          <div className='home-footer-container'>
            <h5>Bakely</h5>
            <Link to='/about-us'>Contact Us</Link>
            <Link to='/faq'>F.A.Q.</Link>
          </div>
          {/*<div className='home-footer-container'>
            <h5>Follow Us</h5>
            <p>Facebook</p>
            <p>Instagram</p>
          </div>*/}
        </div>
        {/*
          <div className='home-page-footer'>
            <Col sm={5} smOffset={1}>
              <h5>Bakely</h5>
              <p>Contact Us</p>
              <p>F.A.Q.</p>
            </Col>
            <Col sm={5}>
              <h5>Follow Us</h5>
              <p>Facebook</p>
              <p>Instagram</p>
            </Col>
          </div>
        */}
      </div>
    )
  }
}
