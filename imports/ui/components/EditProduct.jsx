//BASIC IMPORTS//
import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { browserHistory } from 'react-router';

//UI RELATED IMPORTS

  //Boostrap
  import Alert from 'react-bootstrap/lib/Alert';
  import Button from 'react-bootstrap/lib/Button';
  import Col from 'react-bootstrap/lib/Col';
  import ControlLabel from 'react-bootstrap/lib/ControlLabel';
  import FormGroup from 'react-bootstrap/lib/FormGroup';
  import FormControl from 'react-bootstrap/lib/FormControl';
  import Image from 'react-bootstrap/lib/Image';
  import InputGroup from 'react-bootstrap/lib/InputGroup';

  //Components
  //import ImageCropper1 from './ImageCropper-1.jsx';

  //Methods
  import { saveEditedProduct } from '../../api/methods.jsx';
  import { toggleInvisible } from '../../api/methods.jsx';

//COLLECTION IMPORTS

export default class EditProduct extends TrackerReact(Component) {

  constructor(props) {
    super(props)

    this.handleSelect = this.handleSelect.bind(this);

    this.state = {
      name: this.props.product.name,
      price: this.props.product.price,
      readyIn: this.props.product.readyIn,
      description: this.props.product.description,
      nameLength: this.props.product.name.length,
      descLength: this.props.product.description.length,
      photoLink: this.props.product.image,
      editPicture: false,
      subscription: {
        products: Meteor.subscribe('products')
      }
    }
  }

  componentWillUnmount() {
    this.state.subscription.products.stop();
  }

  cancel() {
    toggleInvisible.call({
      id: this.props.product._id,
      visibility: true,
    }, (error) => {
      if (error) {
        console.log('Error toggling visibility');
      }
    })
    browserHistory.push('/my-shop');
    //FlowRouter.go("my-shop");
  }

  editImage() {
    browserHistory.push(`/edit-product-image/${this.props.product._id}`);
    //FlowRouter.go('/edit-product-image/' + this.props.product._id)
  }

  handleSave() {
    let editedName = this.state.name.trim();
    let editedDesc = this.state.description.trim();
    let editedPrice = parseFloat(this.state.price).toFixed(2);
    let editedReadyIn = this.state.readyIn;
    saveEditedProduct.call({
      productId: this.props.product._id,
      editProductName: editedName,
      editProductPrice: editedPrice,
      editProductReadyIn: editedReadyIn,
      editProductDesc: editedDesc,
    }, (error) => {
      if (error) {
        Bert.alert(error, 'danger');
      } else {
        Bert.alert("Success", 'success');
        toggleInvisible.call({
          id: this.props.product._id,
          visibility: true,
        }, (error) => {
          if (error) {
            console.log('Error toggling visibility');
          }
        })
        browserHistory.push('/my-shop');
        //FlowRouter.go('my-shop');
      }
    })

  }

  writingDesc(event) {
    event.preventDefault();
    this.setState({
      description: event.target.value,
      descLength: event.target.value.trim().length
    })
  }

  writingName(event) {
    event.preventDefault();
    this.setState({
      name: event.target.value,
      nameLength: event.target.value.trim().length
    })
  }

  writingPrice(event) {
    event.preventDefault();
    this.setState({
      price: event.target.value
    })
  }

  handleSelect(event) {
    this.setState({
      readyIn: event.target.value,
    })
  }

  render() {

    let productImage = this.props.product.pendingImg? this.props.product.pendingImg : this.props.product.image;

    let nameWarning = (this.state.nameLength > 40)? <Alert bsStyle='danger'>Exceeded max length</Alert> : "";

    let saveState = (this.state.nameLength > 40)? true : false;

    return (
      <div id="new-product-container">
        <Col xs={10} xsOffset={1} md={3} mdOffset={1}>
          <Image src={productImage} rounded />
          <br />
          <Button className="edit-image-btn" onClick={this.editImage.bind(this)}>Edit Image</Button>
          <hr />
        </Col>
        <Col  xs={8} xsOffset={1} sm={5} md={5} mdOffset={0}>
          <FormGroup controlId="new-product-name">
            <p>Name:</p>
            <FormControl type="text" value={this.state.name} onChange={this.writingName.bind(this)} />
          </FormGroup>
          <p id="new-product-paragraph">{this.state.nameLength}/40</p>
          {nameWarning}
          <hr />
          <FormGroup controlId="new-product-price">
            <p>Price:</p>
            <InputGroup>
              <InputGroup.Addon>$</InputGroup.Addon>
              <FormControl value={this.state.price} onChange={this.writingPrice.bind(this)} type="text" />
            </InputGroup>
          </FormGroup>

          <FormGroup controlId="readyIn">
            <ControlLabel>Ready In:</ControlLabel>
            <FormControl onChange={this.handleSelect} componentClass="select" defaultValue={this.state.readyIn}>
              <option value="0 - 24h">0 - 24h</option>
              <option value="1 - 3 days">1 - 3 days</option>
              <option value="More than 3 days">More than 3 days</option>
            </FormControl>
          </FormGroup>

          <hr />
          <FormGroup controlId="new-product-description">
            <p>Description:</p>
            <textarea id="desc-textarea" className="form-control" rows="3" value={this.state.description} onChange={this.writingDesc.bind(this)} />
          </FormGroup>
          <hr />
          <Button onClick={this.cancel.bind(this)}>Cancel</Button>&emsp;
          <Button onClick={this.handleSave.bind(this)} disabled={saveState}>Save</Button>
        </Col>
      </div>
    )
  }
}
