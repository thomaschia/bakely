//BASIC IMPORTS//
import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
import ProductDetails from './ProductDetails.jsx';
import ProductShopHeader from './ProductShopHeader.jsx';
  //BOOTSTRAP IMPORTS

//COLLECTION IMPORTS
import { Bakeshop } from '../../api/collections/bakeshop.js';
import { Products } from '../../api/collections/products.js';

export default class ProductDetailsComponent extends TrackerReact(Component) {

  constructor() {
    super()

    /*this.state = {
      subscription: {
        products: Meteor.subscribe('products'),
        bakeshop: Meteor.subscribe('bakeshop')
      }
    }
    */
  }

  componentWillUnmount() {
    //this.state.subscription.products.stop();
    //this.state.subscription.bakeshop.stop();
  }

  product() {
    return Products.findOne({_id: this.props.params.id});
  }

  moreProducts() {
    return Products.find({shop: this.product().shop}, {limit: 3}).fetch();
  }

  shop() {
    let product = this.product().shop;
    return Bakeshop.findOne({_id: product});
  }

  render() {

    let shopHandle = Meteor.subscribe('bakeshop');
    let productHandle = Meteor.subscribe('products');

    if (!shopHandle.ready() || !productHandle.ready()) {
      return (<div>Loading...</div>)
    }

    let product = <ProductDetails product={this.product()} shop={this.shop()} />
    let header = <ProductShopHeader shop={this.shop()} more={this.moreProducts()} />

    return (
      <div className='generic-container'>
        {header}
        {product}
      </div>
    )
  }
}
