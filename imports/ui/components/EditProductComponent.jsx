//BASIC IMPORTS//
import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { browserHistory } from 'react-router';

//UI RELATED IMPORTS
import EditProduct from './EditProduct.jsx';
import RegLogin from './RegLogin.jsx';

//COLLECTION IMPORTS
import { Products } from '../../api/collections/products.js';

export default class EditProductComponent extends TrackerReact(Component) {

  constructor() {
    super()

    this.state = {
      subscription: {
        products: Meteor.subscribe('products')
      }
    }
  }

  componentWillUnmount() {
    this.state.subscription.products.stop();
  }

  render() {

    let product = Products.findOne({_id: this.props.params.id})

    if (!Meteor.userId()) {
      return (<RegLogin />)
    } else if (!product) {
      return (<div>Loading...</div>)
    } else if (Roles.subscription.ready()) {
      if (Roles.userIsInRole(Meteor.userId(), 'consumer') || !Roles.userIsInRole(Meteor.userId(), 'vendor-verified') || product.owner != Meteor.userId()) {
        browserHistory.push('/my-account');
        //FlowRouter.go('my-account');
        return null;
      } else {
        return (<div><EditProduct product={product} /></div>) //HERE IS SUCCESS
      }
    } else {
      return (<div>Loading...</div>)
    }
  }
}
