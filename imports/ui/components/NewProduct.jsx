//BASIC IMPORTS//
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';

//UI RELATED IMPORTS
  //BOOTSTRAP
  import Alert from 'react-bootstrap/lib/Alert';
  import Button from 'react-bootstrap/lib/Button';
  import Col from 'react-bootstrap/lib/Col';
  import ControlLabel from 'react-bootstrap/lib/ControlLabel';
  import FormGroup from 'react-bootstrap/lib/FormGroup';
  import FormControl from 'react-bootstrap/lib/FormControl';
  import InputGroup from 'react-bootstrap/lib/InputGroup';

  //COMPONENTS
  import ImageCropper from './ImageCropper.jsx';
  import RegLogin from './RegLogin.jsx';

//METHODS
import { insertNewProduct } from '../../api/methods.jsx';
import { deleteProduct } from '../../api/methods.jsx';

//COLLECTION IMPORTS

export default class NewProduct extends Component {

  constructor(props) {
    super(props)

    this.handleSelect = this.handleSelect.bind(this);

    this.state = {
      name: this.props.product.name,
      price: this.props.product.price,
      readyIn: this.props.product.readyIn,
      description: this.props.product.description,
      nameLength: 0,
      descLength: 0
    }

  }

  componentWillUnmount() {
  }

  cancel() {
    if (this.props.product._id) {
      deleteProduct.call({
        productId: this.props.product._id
      }, (error) => {
        if (error) {
          Bert.alert('Error: please contact us', 'danger');
          throw new Meteor.Error('delete-error', 'error deleting item');
        }
      })
    }
    browserHistory.push('/my-shop');
  }

  nextStep() {
    if (!this.state.name || !this.state.price || !this.state.readyIn || !this.state.description) {
      Bert.alert('Please fill in all fields', 'danger');
      throw new Meteor.Error('empty-fields', 'One or more fields is empty');
    }
    insertNewProduct.call({
      name: this.state.name,
      price: parseFloat(this.state.price).toFixed(2),
      readyIn: this.state.readyIn,
      desc: this.state.description
    }, (error, data) => {
      if (error) {
        Bert.alert('Error proceeding, please check your inputs', 'danger');
        throw new Meteor.Error(error, 'Error inserting new product into db')
      } else {
        browserHistory.push(`/new-product-image/${data}`);
        //FlowRouter.go('/new-product-image/' + data)
      }
    })
  }

  writingDesc(event) {
    event.preventDefault();
    this.setState({
      description: event.target.value,
      descLength: event.target.value.trim().length
    })
  }

  writingName(event) {
    event.preventDefault();
    this.setState({
      name: event.target.value,
      nameLength: event.target.value.trim().length
    })
  }

  writingPrice(event) {
    event.preventDefault();
    this.setState({
      price: event.target.value
    })
  }

  handleSelect(event) {
    this.setState({
      readyIn: event.target.value
    })
  }

  render() {

    let nameWarning = (this.state.nameLength > 40)? <Alert bsStyle='danger'>Exceeded max length</Alert> : "";

    let nextState = (this.state.nameLength > 40 || this.state.nameLength == 0 || this.state.descLength == 0)? true : false;

    if (!Meteor.userId()) {
      return (<RegLogin />)
    } else if (!Roles.subscription.ready()) {
      return (<div>Loading...</div>)
    }

    if (Roles.userIsInRole(Meteor.userId(), 'consumer') || !Roles.userIsInRole(Meteor.userId(), 'vendor-verified')) {
      FlowRouter.go('my-account');
      return null;
    } else {
      return (
        <div id="new-product-container">
          <Col  xs={10} xsOffset={1} sm={6} smOffset={3}>
            <FormGroup controlId="new-product-name">
              <p>Name:</p>
              <FormControl type="text" value={this.state.name} onChange={this.writingName.bind(this)} />
            </FormGroup>
            <p id="new-product-paragraph">{this.state.nameLength}/40</p>
            {nameWarning}
            <hr />
            <FormGroup controlId="new-product-price">
              <p>Price:</p>
              <InputGroup>
                <InputGroup.Addon>$</InputGroup.Addon>
                <FormControl type="text" value={this.state.price} onChange={this.writingPrice.bind(this)} />
              </InputGroup>
            </FormGroup>
            <hr />

            <FormGroup controlId="readyIn">
              <ControlLabel>Ready In:</ControlLabel>
              <FormControl onChange={this.handleSelect} componentClass="select" placeholder="0 - 24h">
                <option value="0 - 24h">0 - 24h</option>
                <option value="1 - 3 days">1 - 3 days</option>
                <option value="More than 3 days">More than 3 days</option>
              </FormControl>
            </FormGroup>

            <hr />
            <FormGroup controlId="new-product-description">
              <p>Description:</p>
              <textarea id="desc-textarea" className="form-control" rows="3" value={this.state.description} onChange={this.writingDesc.bind(this)} />
            </FormGroup>
            <hr />
            <Button onClick={this.cancel.bind(this)}>Cancel</Button>&emsp;
            <Button onClick={this.nextStep.bind(this)} disabled={nextState}>Next</Button>
          </Col>
        </div>
      )
    }
  }
}
