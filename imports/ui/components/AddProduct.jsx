//BASIC IMPORTS//
import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';

//UI RELATED IMPORTS
import Button from 'react-bootstrap/lib/Button';

export default class ProductEdit extends Component {

  addProduct() {
    browserHistory.push('/new-product/new');
    //FlowRouter.go("/new-product/new");
  }

  render() {

    return (
      <div className="add-products-container">
        <div className="add-products-edit-btn">
          <Link to={'/new-product/new'}>Add Product</Link>
        </div>
      </div>
    )
  }
}
