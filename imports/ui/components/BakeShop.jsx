//BASIC IMPORTS
import React, { Component } from 'react';
import { browserHistory } from 'react-router';

//UI RELATED IMPORTS
  //BOOTSTRAP
  import Col from 'react-bootstrap/lib/Col';
  import Image from 'react-bootstrap/lib/Image';
  import Thumbnail from 'react-bootstrap/lib/Thumbnail';

export default class BakeShop extends Component {

  constructor() {
    super()

    this.loaded = this.loaded.bind(this);

    this.state = {
      isLoading: true,
    }
  }

  goToShop() {
    browserHistory.push(`/shops/${this.props.shop._id}`);
    //FlowRouter.go("/shops/"+this.props.shop._id);
  }

  loaded() {
    this.setState({
      isLoading: false,
    })
  }

  render() {

    var shopImg;

    if (!this.props.shop.shop_img) {
      shopImg = '/images/placeholder.png';
    } else {
      shopImg = this.props.shop.shop_img;
    }

    return (
      <Col id="shop-div" lg={3} md={4} sm={4} xs={6} lgOffset={0} mdOffset={0} smOffset={0} xsOffset={3}>
        <Thumbnail onClick={this.goToShop.bind(this)}>
          <div className="shop-container-2">
            <div className="gradient">
            </div>
            <h2>{this.props.shop.name}</h2>
            {/*{this.state.isLoading? <div className='spinner-container-absolute'><div className='spinner'></div></div> : ''}*/}
            <Image src={shopImg} onLoad={this.loaded} alt="shop image" rounded />
          </div>
          <p>{this.props.shop.description}</p>
        </Thumbnail>
      </Col>
    )

    {/*return (
      <div className="bakeshop-div">
      <Col lg={3} md={4} sm={4} xs={10} lgOffset={0} mdOffset={0} smOffset={0} xsOffset={1}>
        <Thumbnail>
          <div className="shop-container" onClick={this.goToShop.bind(this)}>
            <div className="gradient">
            </div>
            <h2>{this.props.shop.name}</h2>
            <Image src={shopImg} alt="shop image" rounded />
          </div>
          <p>{this.props.shop.description}</p>
        </Thumbnail>
      </Col>
    </div>
    )*/}
  }
}
