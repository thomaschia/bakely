//BASIC IMPORTS//
import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //Components
  import NewShop from './NewShop.jsx';

//COLLECTION IMPORTS
import { Bakeshop } from '../../api/collections/bakeshop.js';


export default class NewShopContainer extends TrackerReact(Component) {

  constructor() {
    super()

    this.state = {
      subscription: {
        bakeshop: Meteor.subscribe('bakeshop')
      }
    }
  }

  componentWillUnmount() {
    this.state.subscription.bakeshop.stop();
  }

  render() {

    if (this.props.params.id == 'new') {
      let shop = {
        name: '',
        description: ''
      }
      return (<NewShop shop={shop} />)
    } else {
      let shop = Bakeshop.findOne({_id: this.props.params.id})
      return (<NewShop shop={shop} />)
    }
  }
}
