//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { Link } from 'react-router';

//UI RELATED IMPORTS
  //BOOTSTRAP
  import Button from 'react-bootstrap/lib/Button';
  import Label from 'react-bootstrap/lib/Label';

export default class VendorAccountMain extends TrackerReact(Component) {

  myShop() {
    //FlowRouter.go("my-shop");
  }

  render() {

    let vendorVerified = Roles.userIsInRole(Meteor.userId(), 'vendor-verified')? <h4 className="make-inline"><Label bsStyle="success">verified</Label></h4> : <h4 className="make-inline"><Label bsStyle="warning">not verified</Label></h4>;

    let goToMyShop = Roles.userIsInRole(Meteor.userId(), 'vendor-verified')? <Link to={'/my-shop'}>Go to my Shop</Link> : "Awaiting Verification";

    return (
      <div className='vendor-account-main'>
        <p>Your vendor account is</p>&ensp;{vendorVerified}<br />
        <div className='go-shop-button'>
          {goToMyShop}
        </div>
      </div>
    )
  }
}
