//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //Bootstrap
  import Col from 'react-bootstrap/lib/Col';

export default class AboutUs extends Component {

  render() {

    return (
      <div className='about-us-div'>
        <div>
          <h3>About Us</h3>
          <p>Desserts are always popular amongst the young and the old. In the recent years, we have seen the numbers of home based bakers and demands for customised cakes for specific events.</p>
          <br />
          <p>We are a team of entrepreneurs wanting to make a difference in the baking community in Singapore. Making a difference by helping bakers and baking product sellers to reach out to more customers than they ever could through their own social media platform.</p>
          <br />
          <p>BAKELY was founded with two simple goal: to provide customers with a wide variety of bakes at one place and to house bakers all round Singapore on one single platform.</p>
          <br />
          <p>Our founder, Ian Tan, and his family found it hard to purchase unique cakes for birthdays and unique cake slices for a stay home Sunday. This is where we discovered that if only there was a place where we can search for the specific cakes we want with a specific budget or even supporting local baking talents who are baking from home.</p>
          <br />
          <p>Bakely is a platform that links bakers who are passionate to consumers. From cakes to cupcakes to swissrolls, retailers, home based bakers and seasonal (festive) bakers. Bakely aims to be your one stop platform for all your baking needs.</p>
          <hr />
          <h5>Send us a message</h5>
          <p>Check our FAQ to see if your question has already been answered!</p>
          <div>
            <input type='text' placeholder='Name' />
          </div>
          <div>
            <input type='email' placeholder='Email' />
          </div>
          <div>
            <input type='textarea' placeholder='Message' />
          </div>
        </div>
      </div>
    )
  }
}
