//BASIC IMPORTS//
import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //Components
  import ImageCropper from './ImageCropper.jsx';

//COLLECTION IMPORTS
import { Products } from '../../api/collections/products.js';


export default class NewProductImageContainer extends TrackerReact(Component) {

  constructor() {
    super()

    this.state = {
      subscription: {
        products: Meteor.subscribe('products')
      }
    }
  }

  componentWillUnmount() {
    this.state.subscription.products.stop();
  }

  render() {

    let product = Products.find({_id: this.props.params.id}).fetch();
    if (!product) {
      return (
        <div>Loading...</div>
      )
    }
    let id = product[0]._id;
    let image = product[0].image;

    return (
        <ImageCropper type={'new-product'} id={id} image={image} />
    )
  }
}
