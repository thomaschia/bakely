//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import classNames from 'classnames';

export default class SearchPanel extends TrackerReact(Component) {

  constructor() {
    super()

    this.state = {
      showPanel: true,
    }

    this.handleChange = this.handleChange.bind(this);
    this.togglePanel = this.togglePanel.bind(this);
  }

  handleChange(event) {
    this.props.update(event.target.value);
  }

  togglePanel() {
    let panelShown = this.state.showPanel;
    this.setState({
      showPanel: !panelShown
    });
  }

  render() {

    let panelForm = classNames({
      'display-panel' : this.state.showPanel
    })

    return (
      <div className='search-panel-container'>
        <div className='search-panel-header'>
          <p>{this.props.options.title}</p>
          <p onClick={this.togglePanel}>▼</p>
        </div>
        <form className={panelForm}>
          <label><input type='radio' name='choice' value={this.props.options.buttonOne} onChange={this.handleChange} defaultChecked={true} /><span>{this.props.options.buttonOne}</span></label><br />
          <label><input type='radio' name='choice' value={this.props.options.buttonTwo} onChange={this.handleChange} /><span>{this.props.options.buttonTwo}</span></label><br />
          <label><input type='radio' name='choice' value={this.props.options.buttonThree} onChange={this.handleChange} /><span>{this.props.options.buttonThree}</span></label><br />
          <label><input type='radio' name='choice' value={this.props.options.buttonFour} onChange={this.handleChange} /><span>{this.props.options.buttonFour}</span></label><br />
        </form>
      </div>
    )
  }
}
