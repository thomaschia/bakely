//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

//UI RELATED IMPORTS
  //BOOTSTRAP
  import Button from 'react-bootstrap/lib/Button';
  import Col from 'react-bootstrap/lib/Col';
  import FormControl from 'react-bootstrap/lib/FormControl';
  import FormGroup from 'react-bootstrap/lib/FormGroup';
  import Modal from 'react-bootstrap/lib/Modal';

//METHODS
import { changeUsername, changeEmail } from '../../api/methods.jsx';

export default class EditAccountModal extends Component {

  enterPressed(event, editing) {
    if (event.keyCode == 13) {
      $('#saveNewAccount').click();
    }
  }

  /*Function with argument relating to type of field being altered:
  Username, email or password. Different fields have different input types,
  and different methods called to update the database.*/
  saveEdit(editType) {
    //Switch to handle 3 use cases: username, email, password.
    //Refs are set accoridngly
    switch (editType.editing) {
      case "Username":
      //Attempting to submit empty field
      if (!ReactDOM.findDOMNode(editedField).value.trim()) {
        Bert.alert('Username is empty', 'danger', 'fixed-top');
        throw new Meteor.Error('username-empty', 'Username is empty');
      }
      //Call to validated method
      changeUsername.call({
        newUsername: ReactDOM.findDOMNode(editedField).value.trim()
      }, (error, result) => {
        //Callback in case of error from server
        //Throws Error on server
        if (error) {
          console.log(error);
          Bert.alert(error.reason, 'danger');
        } else {
          Bert.alert('Success', 'success');
          //Call parent function to hide the modal overlay
          this.props.onHide();
        }
      });
        break;
      case "Email":
        changeEmail.call({
          oldEmail: Meteor.user().emails[0].address,
          newEmail: ReactDOM.findDOMNode(editedField).value.trim()
        }, (error, result) => {
          if (error) {
            console.log(error);
            Bert.alert(error.reason, 'danger');
          } else {
            Bert.alert('Success', 'success');
            this.props.onHide();
          }
        });
        break;
      case "Password":
        if (!ReactDOM.findDOMNode(oldPassword).value.trim()) {
          Bert.alert('Please type old password', 'danger', 'fixed-top');
          throw new Meteor.Error('old-password-empty', 'Old password is empty');
        } else if (!ReactDOM.findDOMNode(editedField).value.trim() || !ReactDOM.findDOMNode(retypePassword).value.trim()) {
          Bert.alert('Please type new password', 'danger', 'fixed-top');
          throw new Meteor.Error('new-password-empty', 'New password is empty');
        } else if (ReactDOM.findDOMNode(editedField).value.trim() != ReactDOM.findDOMNode(retypePassword).value.trim()) {
          Bert.alert('Password retype does not match', 'danger', 'fixed-top');
          throw new Meteor.Error('password-match-fail', 'New password does not match retype');
        }
        Accounts.changePassword(ReactDOM.findDOMNode(oldPassword).value.trim(), ReactDOM.findDOMNode(editedField).value.trim(), (error) => {
          if (error) {
            Bert.alert(error.reason, 'danger');
          } else {
            Bert.alert('Success', 'success');
            this.props.onHide();
          }
        });
        break;
    }
  }

  render() {

    var editing;
    var inputType;

    switch (this.props.editing) {
      case "username":
        editing = "Username";
        inputType = "text";
        break;
      case "email":
        editing = "Email";
        inputType = "email";
        break;
      case "password":
        editing = "Password";
        inputType = "password";
        break;
    }

    var ifPassword = (this.props.editing == 'password')? <p>Current Password:</p> : "";
    var breakForPass = (this.props.editing == 'password')? <br /> : "";
    var oldPassword = (this.props.editing == 'password')? <FormGroup controlId="oldPassword"><FormControl type="password" onKeyDown={this.enterPressed.bind(this)} /></FormGroup> : "";
    var retypeLabel = (this.props.editing == 'password')? <p>Re-type New Password:</p> : "";
    var retypePassword = (this.props.editing == 'password')? <FormGroup controlId="retypePassword"><FormControl type="password" onKeyDown={this.enterPressed.bind(this)} /></FormGroup> : "";

    return (
      <Modal show={this.props.show} onHide={this.props.onHide} bsSize="sm" aria-labelledby="contained-modal-title-sm" dialogClassName="editModal">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-sm">Edit {editing}</Modal.Title>
        </Modal.Header>
        <Modal.Body id="edit-modal">
          <div className="modal-content-width">
            <FormGroup controlId="editedField">
                {ifPassword}
                {oldPassword}
                <p>New {editing}:</p>
                <FormControl type={inputType} onKeyDown={this.enterPressed.bind(this)} />
                {retypeLabel}
                {retypePassword}
            </FormGroup>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle="primary" id="saveNewAccount" onClick={this.saveEdit.bind(this, {editing})}>Save</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}
