//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import classNames from 'classnames';

//UI RELATED IMPORTS
  //COMPONENTS
  import Product from './Product.jsx';
  import ProductShopHeader from './ProductShopHeader.jsx';

//COLLECTIONS
import { Products } from '../../api/collections/products.js';
import { Bakeshop } from '../../api/collections/bakeshop.js';

export default class Shop extends TrackerReact(Component) {

  constructor() {
    super()

    this.searching = this.searching.bind(this);
    this.panelProducts = this.panelProducts.bind(this);
    this.panelAbout = this.panelAbout.bind(this);
    this.toggleSearch = this.toggleSearch.bind(this);

    this.state = {
      searchQuery: '',
      panel: 'products',
      showSearch: true,
    }

  }

  searching(event) {
    let query = event.target.value;
    this.setState({
      searchQuery: query
    })
  }

  panelProducts() {
    this.setState({
      panel: 'products'
    })
  }

  panelAbout() {
    this.setState({
      panel: 'about'
    })
  }

  toggleSearch() {
    let showSearch = this.state.showSearch;
    this.setState({
      showSearch: !showSearch
    });
  }

  render() {

    let productHandle = Meteor.subscribe('products');
    let shopsHandle = Meteor.subscribe('bakeshop');
    let products = ['Loading products'];
    let shop = {};
    let query = {shop: this.props.params.id, visible: true};
    if (this.state.searchQuery) {
      let regex = new RegExp(this.state.searchQuery, 'i');
      query = {shop: this.props.params.id, visible: true, name: regex};
    }

    if (productHandle.ready()) {
      products = Products.find(query).fetch().map(product => {
        return <Product key={product._id} product={product} />
      })
    }

    if (shopsHandle.ready()) {
      shop = Bakeshop.findOne({_id: this.props.params.id});
    }

    let content = <p>No products available yet, check again soon!</p>
    let about = <div className='shop-about'><p>{shop.description}</p></div>

    let hideSearch = classNames({
      'shop-panel-search-input' : true,
      'invisible' : (this.state.panel == 'about'),
      'display-none' : !this.state.showSearch,
    });

    let magnify = classNames({
      'shop-panel-magnify-container' : true,
      'display-none' : !this.state.showSearch
    });

    let productStyle = classNames({
      'boldFont' : (this.state.panel == 'products'),
      'display-none-1' : !this.state.showSearch,
    })

    let aboutStyle = classNames({
      'boldFont' : (this.state.panel == 'about'),
      'display-none-1' : !this.state.showSearch,
    })

    if (products.length >= 1) {
      content = <div className='shop-content'>
        <div className='shop-products'>
          {products}
        </div>
      </div>
    }

    return (
      <div className='shop-main-container'>
        {(shop && shop.cover_img) ? <img src={shop.cover_img} alt='cover_img' /> : <br />}
        <ProductShopHeader shop={shop} />
        <div className='shop-tab-panel'>
          <p className={productStyle} onClick={this.panelProducts}>Products</p>
          <p className={aboutStyle} onClick={this.panelAbout}>About</p>
          <div className={hideSearch}>
            <input className='shop-search-input' ref='searchInput' type='text' value={this.state.searchQuery} onChange={this.searching} placeholder='Search shop' />
          </div>
          <div className={magnify}>
            <img onClick={this.toggleSearch} src={this.state.showSearch ? '/images/magnify.png' : '/images/delete.png'} alt='magnify_icon' />
          </div>
        </div>
        {(this.state.panel == 'products') ? content : about}
      </div>
    )
  }
}
