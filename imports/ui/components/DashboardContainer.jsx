import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { browserHistory } from 'react-router';

import DashboardComponent from './DashboardComponent.jsx';

import { Bakeshop } from '../../api/collections/bakeshop.js';

import { getUsers } from '../../api/methods.jsx';

export default class DashboardContainer extends TrackerReact(Component) {

  constructor() {
    super()

    this.state = {
      users: []
    }
  }

  componentWillMount() {
    this.getUsers()
  }

  getUsers() {
    getUsers.call({}, (error, result) => {
      if (error) {
        console.log('Error');
      } else {
        this.setState({
          users: result
        })
      }
    })
  }

  render() {
    if (Roles.subscription.ready() && !Roles.userIsInRole(Meteor.userId(), 'admin')) {
      return <div>Denied</div>
    }

    let allShops;

    let shopSub = Meteor.subscribe('bakeshop');
    if (!shopSub.ready()) {
      return <div>Loading</div>
    }
    allShops = Bakeshop.find().fetch()

     return (
      <DashboardComponent refreshUsers={this.getUsers.bind(this)} users={this.state.users} shops={allShops} />
     )
  }
}
