//BASIC IMPORTS//
import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //Components
  import ImageCropper from './ImageCropper.jsx';

//COLLECTION IMPORTS
import { Bakeshop } from '../../api/collections/bakeshop.js';


export default class EditShopImageContainer extends TrackerReact(Component) {

  constructor() {
    super()
/*
    this.state = {
      subscription: {
        bakeshop: Meteor.subscribe('bakeshop')
      }
    }
    */
  }

  componentWillUnmount() {
    //this.state.subscription.bakeshop.stop();
  }

  render() {

    let shopSub = Meteor.subscribe('bakeshop');

    if (!shopSub.ready()) {
      return (
        <div>Loading...</div>
      )
    }
    let shop = Bakeshop.find({_id: this.props.params.id}).fetch();
    let id = shop[0]._id;
    let image = shop[0].shop_img;

    return (
        <ImageCropper type={'shop'} id={id} image={image} />
    )
  }
}
