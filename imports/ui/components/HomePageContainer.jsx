import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

import HomePage from './HomePage.jsx';

import { Products } from '../../api/collections/products.js';
import { Bakeshop } from '../../api/collections/bakeshop.js';

export default class HomePageContainer extends TrackerReact(Component) {

  render() {

    let featuredProduct;
    let featuredShop;

    let shopSub = Meteor.subscribe('bakeshop');
    let productSub = Meteor.subscribe('products');

    if (!shopSub.ready() || !productSub.ready()) {
      return <div>Loading</div>
    }

    featuredProduct = Products.findOne({featured: true});
    featuredShop = Bakeshop.findOne({featured: true});

    return (
      <HomePage product={featuredProduct} shop={featuredShop} />
    )
  }
}
