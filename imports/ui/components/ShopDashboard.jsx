//BASIC IMPORTS//
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { browserHistory } from 'react-router';

//UI RELATED IMPORTS//
  //BOOTSTRAP
  import Button from 'react-bootstrap/lib/Button';
  import MyShop from './MyShop.jsx';

  //COMPONENTS
  import RegLogin from './RegLogin.jsx';

//METHODS
import { getMemberTier } from '../../api/methods.jsx';

//COLLECTION IMPORTS//
import { Bakeshop } from '../../api/collections/bakeshop.js';

export default class ShopDashboard extends TrackerReact(Component) {

  constructor() {
    super()

    this.state = {
      productLimit: 5,
      subscription: {
        bakeshop: Meteor.subscribe('bakeshop')
      }
    }
  }

  componentDidMount() {
    getMemberTier.call({}, (error, result) => {
      if (error) {
        Bert.alert('An error has occured', 'danger');
        console.log(error.reason);
      } else {
        this.setState({
          productLimit: result
        });
      }
    })
  }

  componentWillUnmount() {
    this.state.subscription.bakeshop.stop();
  }

  createShop() {
    browserHistory.push('/new-shop/new');
    //FlowRouter.go('/new-shop/new');
  }

  render() {

    let findShop = Bakeshop.findOne({owner: Meteor.userId()}); //Returns Object

    if (!Meteor.userId()) {
      return (<RegLogin />)
    } else if (Roles.subscription.ready()) {
      if (Roles.userIsInRole(Meteor.userId(), 'consumer') || !Roles.userIsInRole(Meteor.userId(), 'vendor-verified')) {
        browserHistory.push('/my-account');
        //FlowRouter.go('my-account');
        return null;
      } else if (!findShop) {
        return (<div><Button bsStyle='success' onClick={this.createShop.bind(this)}>Create Shop</Button></div>)
      } else {
        return (<div><MyShop shop={findShop} productLimit={this.state.productLimit} /></div>) //HERE IS SUCCESS
      }
    } else {
      return (<div>Loading...</div>)
    }
  }
}
