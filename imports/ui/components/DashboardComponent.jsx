import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import UserPanel from './UserPanel.jsx';

export default class DashboardComponent extends Component {

  componentDidUpdate(nextProps) {
    this.buildPanelArray(nextProps.users, nextProps.shops)
  }

  userIsVendorVerified(array, predicate) {
    array.find((element) => {
      return element === predicate
    })
  }

  findUsersShop(shops, user) {
    return shops.filter((shop) => {
      return shop.owner == user
    })
  }

  sortArray(array) {
    return array.sort((a, b) => {
      return a.verified - b.verified
    })
  }

  buildPanelArray(users, shops) {
    let panelArray = [];
    for (i = 0; i < users.length; i++) {
      if (!users[i].roles.includes('vendor')) {
        console.log('Not a vendor');
      } else {
        let panelObject = {
          userId: users[i]._id,
          username: users[i].username,
          shopId: '',
          verified: false,
          shop: false,
          visible: false
        }
        let usersShop = this.findUsersShop(shops, users[i]._id)
        if (!usersShop[0]) {
          console.log(`${users[i].username} has no shop`);
          panelObject.verified = users[i].roles.includes('vendor-verified')
          panelArray.push(panelObject)
        } else {
          panelObject.shopId = usersShop[0]._id
          panelObject.verified = users[i].roles.includes('vendor-verified')
          panelObject.shop = !!usersShop[0].name
          panelObject.visible = usersShop[0].visible
          panelArray.push(panelObject)
        }
      }
    }
    return this.sortArray(panelArray)
  }

  render() {
    const userPanel = this.buildPanelArray(this.props.users, this.props.shops).map((user, index) => {
      return <UserPanel getUsers={this.props.refreshUsers} key={index} data={user} />
    })

    return (
      <div>
        {userPanel}
      </div>
    )
  }
}
