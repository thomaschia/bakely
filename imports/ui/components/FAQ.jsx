//BASIC IMPORTS
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //Bootstrap
  import Col from 'react-bootstrap/lib/Col';

export default class FAQ extends Component {

  render() {

    return (
      <div className='faq-div'>
        <div>
          <h3>FAQ</h3>
          <h4>How do I make a purchase?</h4>
          <p>As we are a free-to-use platform, purchase enquiries will go directly to the Baker. You can contact the Baker via their Shop Page or Product Page.</p>
          <br />
          <h4>How do I collect the product?</h4>
          <p>The Baker will contact you directly with regards to payment and collection/delivery information.</p>
          <br />
          <h4>Are Bake Shops always open?</h4>
          <p>Some Bakers only operate during specific periods (e.g. festive holidays). You can check the details on their Shop Page.</p>
          <br />
          <h4>Who do I contact for product or after-sales related issues?</h4>
          <p>You can contact the Baker directly via the contact button, or an alternative method suggested by them.</p>
          <br />
          <h4>Is it safe to buy from home bakers?</h4>
          <p>Home Bakers have to adhere to regulations set by NEA. A copy of the guidelines can be found <a href='http://www.nea.gov.sg/docs/default-source/public-health/food-hygiene/Guidelines/guidelines-on-good-hygienic-practices-for-bakeries-and-cakeshops.pdf'>here</a> and <a href='http://www.nea.gov.sg/docs/default-source/public-health/food-hygiene/guidelines.pdf'>here</a>.</p>
          <br />
          <h4>How do I start advertising on Bakely?</h4>
          <p><a href='https://bakely.sg/login'>Sign up</a> for a vendor account.</p>
          <br />
          <h4>Are there any fees to join?</h4>
          <p>No, there are no fees for the free tier. Potential customers will contact you directly at the email address specified when you create the account.</p>
          <br />
        </div>
      </div>
    )
  }
}
