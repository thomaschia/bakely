//BASIC IMPORTS
import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

//UI RELATED IMPORTS
  //COMPONENTS
  import StarRating from './StarRating.jsx';

export default class ProductShopHeader extends TrackerReact(Component) {

  constructor() {
    super()

    this.goToShop = this.goToShop.bind(this);
    //this.goToProduct = this.goToProduct.bind(this);
  }

  goToShop() {
    browserHistory.push(`/shops/${this.props.shop._id}`);
  }

  goToProduct(id) {
    browserHistory.push(`/product-details/${id}`);
  }

  render() {

    console.log(this.props.shop);

    let contactButton = Meteor.userId() ? <Link to={`/contact/${this.props.shop._id}`}>Contact</Link> : <Link to='/login'>Log in to send a message</Link>;

    let moreProducts = this.props.more ? this.props.more.map(product => {
      return <img onClick={() => this.goToProduct(product._id)} key={product._id} src={product.image} alt='more products' />
    }) : <div className='contact-button-container'>{contactButton}</div>;

    return (
      <div className='product-header-container'>
        <div onClick={this.goToShop} className='product-header-logo'>
          <img src={this.props.shop.shop_img} alt='shop-logo' />
        </div>
        <div className='product-header-shop-info'>
          <p onClick={this.goToShop}>{this.props.shop.name}</p>
          <p>Singapore</p>
          <div className='product-header-rating-container'>
            <StarRating date={this.props.shop.createdAt} />
          </div>
        </div>
        <div className='product-header-more-products-container'>
          {moreProducts}
        </div>
      </div>
    )
  }
}
