import { Meteor } from 'meteor/meteor';

Slingshot.fileRestrictions("imageUploads", {
  allowedFileTypes: ["image/png", "image/jpg", "image/jpeg", "image/gif"],
  maxSize: 10 * 1024 * 1024,
});
