import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Accounts } from 'meteor/accounts-base';

import { Bakeshop } from './collections/bakeshop.js';
import { Products } from './collections/products.js';
import { Threads } from './collections/threads.js';
import { Messages } from './collections/messages.js';

const AWSConfigUpdate = {
  accessKeyId: Meteor.settings.AWSAccessKeyId,
  secretAccessKey: Meteor.settings.AWSSecretAccessKey,
  region: Meteor.settings.AWSRegion,
};

export const handleVerify = new ValidatedMethod({
  name: 'handleVerify',
  validate: new SimpleSchema({
    userId: { type: String }
  }).validator(),
  run({userId}) {
    if(Meteor.isServer) {
      let user = Meteor.users.find({_id: userId}).fetch()
      console.log(user);
      if (!user) {
        throw new Meteor.Error('user-not-found', 'User not found')
      } else if (Roles.userIsInRole(user[0]._id, 'vendor-verified')) {
        Roles.removeUsersFromRoles(user[0]._id, 'vendor-verified')
        return
      }
      Roles.addUsersToRoles(user[0]._id, 'vendor-verified')
      return
    }
  }
})

export const handleEnable = new ValidatedMethod({
  name: 'handleEnable',
  validate: new SimpleSchema({
    shopId: { type: String }
  }).validator(),
  run({shopId}) {
    if (Meteor.isServer) {
      let shop = Bakeshop.findOne({_id: shopId})
      if (!shop) {
        throw new Meteor.Error('shop-not-found', 'Shop not found')
      }
      let visibility = shop.visible
      Bakeshop.update({_id: shopId}, {$set: {visible: !visibility}})
    }
  }
})

export const getUsers = new ValidatedMethod({
  name: 'getUsers',
  validate() {
  },
  run() {
    let users;
    if (Meteor.isServer) {
      users = Meteor.users.find().fetch()
    }
    return users;
  }
})

export const getMemberTier = new ValidatedMethod({
  name: 'getMemberTier',
  validate() {
  },
  run() {
    if (Meteor.isServer) {
      let limit = Meteor.settings.FreeProductLimit;
      if (!(Roles.userIsInRole(this.userId), 'vendor-verified')) {
        throw new Meteor.Error('not-authorised', 'Not authorised to perform this action');
      } else if (Roles.userIsInRole(this.userId, 'silver-member')) {
        limit = Meteor.settings.SilverProductLimit;
      } else if (Roles.userIsInRole(this.userId, 'gold-member')) {
        limit = Meteor.settings.GoldProductLimit;
      }
      return limit;
    }
  }
})

export const getDistinctProducts = new ValidatedMethod({
  name: 'getDistinctProducts',
  validate: new SimpleSchema({
    searchQuery: {type: String}
  }).validator(),
  run({searchQuery}) {
    let regex = new RegExp(searchQuery, 'i');
    if (Meteor.isServer) {
      let rawProducts = Products.rawCollection();
      let aggregateQuery = Meteor.wrapAsync(rawProducts.aggregate, rawProducts);
      return aggregateQuery( [
        { $match: { name: regex } },
        { $group: { _id: "$name", total: { $sum: 1} } },
        { $limit: 10 },
      ] );
    }
  }
})

export const deleteProduct = new ValidatedMethod({
  name: 'deleteProduct',
  validate: new SimpleSchema({
    productId: {type: String}
  }).validator(),
  run({productId}) {
    let product = Products.findOne({_id: productId})
    if (Meteor.isServer) {
      if (product.owner != this.userId) {
        throw new Meteor.Error('not-authorised', 'You are not authorised to delete this item');
      }
      Products.remove({_id: productId});
    }
  }
})

export const deleteImages = new ValidatedMethod({
  name: 'deleteImages',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    array: {type: [String]},
  }).validator(),
  run({array}) {
    let toDelete = array.map(url => {
      return { Key: url.replace('https://' + Meteor.settings.BucketName + '.s3-' + Meteor.settings.AWSRegion + '.amazonaws.com/', '') }
    });

    let delParams = {
      Bucket: Meteor.settings.BucketName,
      Delete: {
        Objects: toDelete,
      }
    };
    s3DeletePromise(delParams)
      .then(() => {
        return;
      })
      .catch((error) => {
        throw new Meteor.Error(error);
      })
  }
})

export const insertNewProduct = new ValidatedMethod({
  name: 'insertNewProduct',
  validate: new SimpleSchema({
    name: {type: String, max: 40},
    price: {type: String},
    readyIn: {type: String},
    desc: {type: String}
  }).validator(),
  run({name, price, readyIn, desc}) {
    if (Meteor.isServer) {
      let shop = Bakeshop.findOne({owner: this.userId});
      if (!shop) {
        throw new Meteor.Error('shop-not-found', 'No shop associated with this account');
      }
      let priceFloat = parseFloat(price);
      let id = Products.insert({name: name, price: priceFloat, readyIn: readyIn, description: desc, owner: this.userId, shop: shop._id, visible: false})
      if (!id) {
        throw new Meteor.Error('insert-fail', 'DB insert failed');
      } else {
        return id;
      }
    }
  }
})

export const toggleInvisible = new ValidatedMethod({
  name: 'makeInvisible',
  validate: new SimpleSchema({
    id: {type: String},
    visibility: {type: Boolean},
  }).validator(),
  run({id, visibility}) {
    if (Meteor.isServer) {
      let product = Products.findOne({_id: id});
      if (!product) {
        throw new Meteor.Error('no-item', 'The item does not exist')
      } else if (product.owner != this.userId) {
        throw new Meteor.Error('not-authorised', 'You are not authorised to edit this item')
      }
      Products.update({_id: id}, {$set: {visible: visibility}});
    }
  }
})

export const toggleInvisibleShop = new ValidatedMethod({
  name: 'makeInvisibleShop',
  validate: new SimpleSchema({
    id: {type: String},
    visibility: {type: Boolean},
  }).validator(),
  run({id, visibility}) {
    if (Meteor.isServer) {
      let shop = Bakeshop.findOne({_id: id});
      if (!shop) {
        throw new Meteor.Error('no-shop', 'The shop does not exist')
      } else if (shop.owner != this.userId) {
        throw new Meteor.Error('not-authorised', 'You are not authorised to edit this item')
      }
      Bakeshop.update({_id: id}, {$set: {visible: visibility}});
    }
  }
})


const s3CopyPromise = (parameters) => {
  return new Promise((resolve, reject) => {

    AWS.config.update(AWSConfigUpdate);

    let s3 = new AWS.S3();
    if (Meteor.isServer) {
      s3.copyObject(parameters, Meteor.bindEnvironment(function(error, result) {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      }));
    }
  });
}

const s3DeletePromise = (parameters) => {
  return new Promise((resolve, reject) => {

    AWS.config.update(AWSConfigUpdate);

    let s3 = new AWS.S3();
    if (Meteor.isServer) {
      s3.deleteObjects(parameters, Meteor.bindEnvironment(function (error, result) {
        if (error) {
          reject(error);
        } else {
          resolve(result);
        }
      }))
    }
  });
}

const updateImage = (type, id, url) => {
  return new Promise((resolve, reject) => {

    switch (type) {
      case 'edit-product':
      case 'new-product':
        Products.update({_id: id}, {$set: {image: url, visible: true}}, (error, result) => {
          if (error) {
            reject(error);
          } else {
            resolve();
          }
        })
        break;

      case 'shop':
        Bakeshop.update({_id: id}, {$set: {shop_img: url, visible: true}}, (error, result) => {
          if (error) {
            reject(error);
          } else {
            resolve();
          }
        })
        break;

      case 'cover':
        Bakeshop.update({_id: id}, {$set: {cover_img: url}}, (error, result) => {
          if (error) {
            reject(error);
          } else {
            resolve();
          }
        })
        break;

      default:
        reject();
    }
  });
}

export const saveImage = new ValidatedMethod({
  name: 'saveImage',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    updateType: {type: String},
    id: {type: String},
    urlToDelete: {type: String, optional: true},
    array: {type: [String]},
  }).validator(),
  run({updateType, id, urlToDelete, array}) {

    let updateParam;

    switch(updateType) {
      case 'edit-product':
      case 'new-product':
        updateParam = Products.findOne({_id: id});
        if (updateParam.owner != this.userId) {
          throw new Meteor.Error('not-authorised', 'You are not authorised to edit this item');
        };
        break;
      case 'shop':
      case 'cover':
        updateParam = Bakeshop.findOne({owner: this.userId});
        break;
      default:
        throw new Meteor.Error('Missing parameter', 'Missing update parameter type: product image, shop image, cover image');
    }

    let length = array.length - 1;
    //Map array to appropriate format for S3 parameters (trim bucket name and region)
    let toDelete = array.map(url => {
      return { Key: url.replace('https://' + Meteor.settings.BucketName + '.s3-' + Meteor.settings.AWSRegion + '.amazonaws.com/', '')}
    })
    //If editing image, add old image path to array for deletion
    if (urlToDelete) {
      toDelete.push({  Key: urlToDelete.replace('https://' + Meteor.settings.BucketName + '.s3-' + Meteor.settings.AWSRegion + '.amazonaws.com/', '') });
    }
    //copySource is user/temp/date/filename.jpg
    let copySource = array[length].replace('https://' + Meteor.settings.BucketName + '.s3-' + Meteor.settings.AWSRegion + '.amazonaws.com/', '');
    //destSource is user/date-filename.jpg
    let destSource = copySource.replace('temp/', '');
    //destFullSource is https://bucketname.../user/date-filename.jpg
    //This is for inserting into database
    let destFullSource = array[length].replace('temp/', '');

    let copyParams = {
      Bucket: Meteor.settings.BucketName,
      CopySource: Meteor.settings.BucketName + '/' + copySource,
      Key: destSource
    };

    let delParams = {
      Bucket: Meteor.settings.BucketName,
      Delete: {
        Objects: toDelete,
      }
    };
    //var s3 = new AWS.S3();

    s3CopyPromise(copyParams)
      .then(updateImage.bind(null, updateType, updateParam._id, destFullSource))
      .then(s3DeletePromise.bind(null, delParams))
      .then(() => {
        return;
      })
      .catch((error) => {
        throw new Meteor.Error(error);
      })

  }
})

export const deleteShop = new ValidatedMethod({
  name: 'deleteShop',
  validate: new SimpleSchema({}).validator(),
  run({}) {
    if (Meteor.isServer) {
      let shop = Bakeshop.findOne({owner: this.userId});
      if (shop.owner != this.userId) {
        throw new Meteor.Error('not-authorised', 'You are not authorised to change this item');
      }
      Bakeshop.remove({_id: shop._id});
    }
  }
})

export const insertNewShop = new ValidatedMethod({
  name: 'insertNewShop',
  validate: new SimpleSchema({
    name: {type: String},
    description: {type: String, max: 150},
  }).validator(),
  run({name, description}) {
    if (Meteor.isServer) {
      let shop = Bakeshop.find({owner: this.userId});
      if (!this.userId || !Roles.userIsInRole(this.userId, 'vendor-verified' || shop)) {
        throw new Meteor.Error('not-authorised', 'Not authorised to perform this function')
      }
      return Bakeshop.insert({name: name, description: description, owner: this.userId, visible: false, createdAt: new Date()});
    }
  }
})

export const notsaveImage = new ValidatedMethod({
  name: 'notsaveImage',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    urlToDelete: {type: String, optional: true},
    array: {type: [String]}
  }).validator(),
  run({urlToDelete, array}) {
    if (Meteor.isServer) {
      let shop = Bakeshop.findOne({owner: this.userId});
      if (!shop || shop.owner != this.userId) {
        throw new Meteor.Error('not-authorised', 'Not authorised to perform this function');
      }

      let length = array.length - 1;

      let toDelete = array.map(url => {
        return { Key: url.replace('https://' + Meteor.settings.BucketName + '.s3-' + Meteor.settings.AWSRegion + '.amazonaws.com/', '')}
      })

      if (urlToDelete) {
        toDelete.push({  Key: urlToDelete.replace('https://' + Meteor.settings.BucketName + '.s3-' + Meteor.settings.AWSRegion + '.amazonaws.com/', '') });
      }

      let copySource = array[length].replace('https://' + Meteor.settings.BucketName + '.s3-' + Meteor.settings.AWSRegion + '.amazonaws.com/', '');
      let destSource = copySource.replace('temp/', '');
      let destFullSource = array[length].replace('temp/', '');

      let copyParams = {
        Bucket: Meteor.settings.BucketName,
        CopySource: Meteor.settings.BucketName + '/' + copySource,
        Key: destSource
      };

      let delParams = {
        Bucket: Meteor.settings.BucketName,
        Delete: {
          Objects: toDelete,
        }
      };

      s3CopyObject.call({
        parameters: copyParams,
        callback: function() {
          Bakeshop.update({_id: shop._id}, {$set: {shop_img: destFullSource}});
          s3DeleteObjects.call({
            parameters: delParams,
          }, (error, result) => {
            if (error) {
              console.log(error);
            }
          })
        }
      }, (error, result) => {
        if (error) {
          console.log(error);
        }
      })
    }
  }
})

export const saveDescription = new ValidatedMethod({
  name: 'saveDescription',
  validate: new SimpleSchema({
    shopId: {type: String},
    shopDesc: {type: String, max: 150}
  }).validator(),
  run({shopId, shopDesc}) {
    let shop = Bakeshop.findOne({_id: shopId})
    if (Meteor.isServer) {
      if (shop.owner != this.userId) {
        throw new Meteor.Error('not-authorised', 'You are not authorised to change this item');
      }
      Bakeshop.update({_id: shopId}, {$set: {description: shopDesc}})
    }
  }
})

export const saveName = new ValidatedMethod({
  name: 'saveName',
  validate: new SimpleSchema({
    shopId: {type: String},
    shopName: {type: String, max: 20}
  }).validator(),
  run({shopId, shopName}) {
    let shop = Bakeshop.findOne({_id: shopId})
    if (Meteor.isServer) {
      if (shop.owner != this.userId) {
        throw new Meteor.Error('not-authorised', 'You are not authorised to change this item');
      }
      Bakeshop.update({_id: shopId}, {$set: {name: shopName}})
    }
  }
})

export const saveEditedProduct = new ValidatedMethod({
  name: 'saveEditedProduct',
  validate: new SimpleSchema({
    productId: {type: String},
    editProductName: {type: String, max: 40},
    editProductPrice: {type: String},
    editProductReadyIn: {type: String},
    editProductDesc: {type: String}
  }).validator(),
  run({productId, editProductName, editProductPrice, editProductReadyIn, editProductDesc}) {
    let product = Products.findOne({_id: productId})
    if (Meteor.isServer) {
      if (product.owner != this.userId) {
        throw new Meteor.Error('not-authorised', 'You are not authorised to change this item')
      } else {
        Products.update({_id: productId}, {$set: {name: editProductName, price: editProductPrice, readyIn: editProductReadyIn, description: editProductDesc, visible: true}})
      }
    }
  }
})

export const saveNewProduct = new ValidatedMethod({
  name: 'saveNewProduct',
  validate: new SimpleSchema({
    newProductName: {type: String, max: 40},
    newProductPrice: {type: Number, decimal: true},
    newProductDesc: {type: String},
    newProductURL: {type: String},
  }).validator(),
  run({productId, newProductName, newProductPrice, newProductDesc, newProductURL}) {
    if (Meteor.isServer) {
      let shop = Bakeshop.findOne({owner: this.userId})
      if (!this.userId || !Roles.userIsInRole(this.userId, 'vendor-verified') || !shop) {
        throw new Meteor.Error('not-authorised', 'You are not authorised to perform this function')
      } else {
        Products.insert({name: newProductName, price: newProductPrice, description: newProductDesc, image: newProductURL, owner: this.userId, shop: shop._id})
      }
    }
  }
})

export const sendMail = new ValidatedMethod({
  name: 'sendEmail',
  validate: new SimpleSchema({
    to: {type: String},
    from: {type: String},
    subject: {type: String},
    text: {type: String}
  }).validator(),
  run({to, from, subject, text}) {
    if (Meteor.isServer) {
      Email.send({
        to: to,
        from: from,
        subject: subject,
        text: text
      });
    }
  }
})

export const changeUsername = new ValidatedMethod({
  name: 'changeUsername',
  validate: new SimpleSchema({
    newUsername: {type: String}
  }).validator(),
  run({newUsername}) {
    if (Meteor.isServer) {
      if (Accounts.findUserByUsername(newUsername)) {
        throw new Meteor.Error('username-exists', 'Username is already taken');
      }
      Accounts.setUsername(this.userId, newUsername);
    }
  }
});

export const changeEmail = new ValidatedMethod({
  name: 'changeEmail',
  validate: new SimpleSchema({
    oldEmail: {type: String},
    newEmail: {type: String}
  }).validator(),
  run({oldEmail, newEmail}) {
    if (Meteor.isServer) {
      if (Accounts.findUserByEmail(newEmail)) {
        throw new Meteor.Error('email-exists', 'Email is already is in use');
      }
      Accounts.addEmail(this.userId, newEmail);
      Accounts.removeEmail(this.userId, oldEmail);
    }
  }
});

export const registerNewUser = new ValidatedMethod({
  name: 'register',
  validate: new SimpleSchema({
    username: { type: String },
    password: { type: String },
    email: { type: String },
    account: { type: Boolean }
  }).validator(),
  run({ username, password, email, account }) {
    //throw new Meteor.Error('not-authorised', 'Not authorised to set up new account (yet)');
    if (Meteor.isServer) {
      SSR.compileTemplate('vendorEmail', Assets.getText('vendorEmail.html'));
      let emailData = {
        username: username
      }
      const userExists = Accounts.findUserByUsername(username);
      const emailExists = Accounts.findUserByEmail(email);
      if (userExists) {
        throw new Meteor.Error('username-exists', 'Username is already taken');
      }
      if (emailExists) {
        throw new Meteor.Error('email-exists', 'Email is already in use');
      }
      var id = Accounts.createUser({
        username: username,
        email: email,
        password: password
      });
      SSR.compileTemplate('verify', Assets.getText('verify.html'));
      let verificationData = {
        id: id,
        username: username,
        email: email
      };
      if (account && !Roles.userIsInRole(id, 'vendor')) {
        Roles.addUsersToRoles(id, 'vendor');
        Email.send({
          from: "Bakely SG admin@bakely.sg",
          to: email,
          subject: 'Welcome to Bakely',
          html: SSR.render('vendorEmail', emailData)
        });
        Email.send({
          from: "Bakely SG admin@bakely.sg",
          to: Meteor.settings.MasterEmail,
          subject: 'New Registration at Bakely.sg',
          html: SSR.render('verify', verificationData)
        });
      } else if (!account && !Roles.userIsInRole(id, 'consumer')) {
        Roles.addUsersToRoles(id, 'consumer');
      } else {
        throw new Meteor.Error('already-in-role', 'Account is already associated with either vendor or consumer');
      }
    }
  }
})

export const forgotLogin = new ValidatedMethod({
  name: 'forgotLogin',
  validate: new SimpleSchema({
    email: {type: String},
  }).validator(),
  run({email}) {
    if (Meteor.isServer) {
      if (!email) {
        throw new Meteor.Error('no-email-entered', 'Please enter an email');
      }
      let user = Accounts.findUserByEmail(email);
      if (!user) {
        throw new Meteor.Error('not-found', 'Email record not found');
      }
      Accounts.sendResetPasswordEmail(user._id);
    }
  }
})

export const sendMessage = new ValidatedMethod({
  name: 'sendMessage',
  validate: new SimpleSchema({
    thread: {type: String},
    to: {type: String},
    message: {type: String},
  }).validator(),
  run({thread, to, message}) {
    let currentUser = this.userId;
    if (!currentUser) {
      throw new Meteor.Error('not-logged-in', 'User not logged in');
    }
    if (Meteor.isServer) {
      let toUser = Meteor.users.findOne({_id: to});
      if (!toUser) {
        throw new Meteor.Error('user-does-not-exist', 'User does not exist');
      }
      let threadId = Threads.findOne({_id: thread});
      if (!threadId) {
        Threads.insert({user1: currentUser, user2: to, timestamp: new Date()});
      }
      Messages.insert({from: currentUser, to: to, thread: thread, message: message, timestamp: new Date(), read: false});
      Threads.update({_id: thread}, {$set: {timestamp: new Date(), unread: true}});
    }
  }
})

export const messageRead = new ValidatedMethod({
  name: 'messageRead',
  validate: new SimpleSchema({
    msgId: {type: String},
  }).validator(),
  run({msgId}) {
    if (Meteor.isServer) {
      let msg = Messages.findOne({_id: msgId});
      if (!msg) {
        throw new Meteor.Error('msg-not-found', 'The message cannot be found on the server');
      }
      Messages.update({_id: msgId}, {$set: {read: true}});
      let msgsInThread = Messages.find({thread: msg.thread, read: false}).fetch();
      if (msgsInThread.length > 0) {
        Threads.update({_id: msg.thread}, {$set: {unread: true}});
      } else {
        Threads.update({_id: msg.thread}, {$set: {unread: false}});
      }

    }
  }
})

export const createThread = new ValidatedMethod({
  name: 'createThread',
  validate: new SimpleSchema({
    id: {type: String},
  }).validator(),
  run({id}) {
    let currentUser = this.userId;
    if (!currentUser) {
      throw new Meteor.Error('not-logged-in', 'User not logged in');
    }
    if (currentUser == id) {
      throw new Meteor.Error('message-self', 'You cannot message yourself!');
    }
    if (Meteor.isServer) {
      let thread1 = Threads.findOne({$and: [{user1: this.userId}, {user2: id}]});
      if (thread1) {
        return thread1._id;
      }
      let thread2 = Threads.findOne({$and: [{user1: id}, {user2: this.userId}]});
      if (thread2) {
        return thread2._id;
      }
      return Threads.insert({user1: this.userId, user2: id, timestamp: new Date(), unread: false});
    }
  }
})

export const sendContactMessage = new ValidatedMethod({
  name: 'sendContactMessage',
  validate: new SimpleSchema({
    to: {type: String},
    subject: {type: String},
    message: {type: String},
  }).validator(),
  run({to, subject, message}) {
    if (Meteor.isServer) {
      let ownerId = Bakeshop.findOne({_id: to}).owner;
      let ownerEmail = Meteor.users.findOne({_id: ownerId}).emails[0].address;
      Email.send({
        to: ownerEmail,
        from: Meteor.user().emails[0].address,
        subject: subject,
        message: message
      })
    }
  }
})
