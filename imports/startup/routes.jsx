/*
import React from 'react';
import { mount } from 'react-mounter';

import Main from '../ui/layouts/Main.jsx';
import MainNoNav from '../ui/layouts/MainNoNav.jsx';
import HomePage from '../ui/components/HomePage.jsx';
import AboutUs from '../ui/components/AboutUs.jsx';
import ShopsHome from '../ui/components/ShopsHome.jsx';
import Shop from '../ui/components/Shop.jsx';
import ImageCropperContainer from '../ui/components/ImageCropperContainer.jsx';
import ImageCropperContainer2 from '../ui/components/ImageCropperContainer2.jsx';
import ImageCropperContainer3 from '../ui/components/ImageCropperContainer3.jsx';
import ImageCropperContainer4 from '../ui/components/ImageCropperContainer4.jsx';
import RegLogin from '../ui/components/RegLogin.jsx';
import ForgotLogin from '../ui/components/ForgotLogin.jsx';
import ResetLogin from '../ui/components/ResetLogin.jsx';
import Account from '../ui/components/Account.jsx';
import EditAccount from '../ui/components/EditAccount.jsx';
import ShopDashboard from '../ui/components/ShopDashboard.jsx';
import NewProductContainer from '../ui/components/NewProductContainer.jsx';
import EditProductComponent from '../ui/components/EditProductComponent.jsx';
import ProductDetailsComponent from '../ui/components/ProductDetailsComponent.jsx';
import Search from '../ui/components/Search.jsx';
import SearchResults from '../ui/components/SearchResults.jsx';
import MessagesContainer from '../ui/components/MessagesContainer.jsx';
import NewShopContainer from '../ui/components/NewShopContainer.jsx';

FlowRouter.route('/', {
  action() {
    mount(Main, {
      content: (<HomePage />)
    })
  },
  name: "home"
});
//DONE

FlowRouter.route('/all-shops', {
  action() {
    mount(Main, {
      content: (<ShopsHome />)
    })
  },
  name: "all-shops"
});
//DONE

FlowRouter.route('/about-us', {
  action() {
    mount(Main, {
      content: (<AboutUs />)
    })
  },
  name: "about-us"
});
//DONE

FlowRouter.route('/shops/:shopName', {
  action(params, queryParams) {
    mount(Main, {
      content: (<Shop id={params.shopName} />)
    })
  },
  name: "shop"
});
//DONE

FlowRouter.route('/crop', {
  action() {
    mount(Main, {
      content: (<ImageCropper />)
    })
  },
  name: "crop"
});
//NOT NEEDED

FlowRouter.route('/login', {
  action() {
    mount(Main, {
      content: (<RegLogin />)
    })
  },
  name: "login"
});
//DONE

FlowRouter.route('/my-account', {
  action() {
    mount(Main, {
      content: (<Account />)
    })
  },
  name: "my-account"
});
//DONE

FlowRouter.route('/search', {
  action() {
    mount(Main, {
      content: (<Search />)
    })
  },
  name: "search"
});
//NOT NEEDED

FlowRouter.route('/search-products/:query', {
  action(params, queryParams) {
    mount(Main, {
      content: (<SearchResults query={params.query} />)
    })
  },
  name: "search-products"
});
//DONE

FlowRouter.route('/my-shop', {
  action() {
    mount(Main, {
      content: (<ShopDashboard />)
    })
  },
  name: "my-shop"
});
//DONE

FlowRouter.route('/new-shop/:shop_id', {
  action(params, queryParams) {
    mount(MainNoNav, {
      content: (<NewShopContainer id={params.shop_id} />)
    })
  },
  name: "new-shop"
});
//DONE

FlowRouter.route('/edit-shop-image/:shop_id', {
  action(params, queryParams) {
    mount(MainNoNav, {
      content: (<ImageCropperContainer3 id={params.shop_id} />)
    })
  },
  name: "edit-shop-image"
});
//DONE

FlowRouter.route('/new-shop-image/:shop_id', {
  action(params, queryParams) {
    mount(MainNoNav, {
      content: (<ImageCropperContainer4 id={params.shop_id} />)
    })
  },
  name: "new-shop-image"
});
//DONE

FlowRouter.route('/new-product/:product_id', {
  action(params, queryParams) {
    mount(MainNoNav, {
      content: (<NewProductContainer id={params.product_id} />)
    })
  },
  name: "new-product"
});
//DONE

FlowRouter.route('/new-product-image/:product_id', {
  action(params, queryParams) {
    mount(MainNoNav, {
      content: (<ImageCropperContainer2 id={params.product_id} />)
    })
  },
  name: "new-product-image"
});
//DONE

FlowRouter.route('/edit-product/:product_id', {
  action(params, queryParams) {
    mount(MainNoNav, {
      content: (<EditProductComponent id={params.product_id}/>)
    })
  },
  name: "edit-product"
});
//DONE

FlowRouter.route('/edit-product-image/:product_id', {
  action(params, queryParams) {
    mount(MainNoNav, {
      content: (<ImageCropperContainer id={params.product_id}/>)
    })
  },
  name: "edit-product-image"
});
//DONE

FlowRouter.route('/product-details/:product_id', {
  action(params, queryParams) {
    mount(Main, {
      content: (<ProductDetailsComponent id={params.product_id} />)
    })
  },
  name: "product-details"
});
//DONE

FlowRouter.route('/forgot', {
  action() {
    mount(Main, {
      content: (<ForgotLogin />)
    })
  },
  name: "forgot-login"
});
//DONE

FlowRouter.route('/reset-password/:token', {
  action(params, queryParams) {
    mount(Main, {
      content: (<ResetLogin token={params.token}/>)
    })
  },
  name: "reset-password"
});
//DONE

/*
FlowRouter.route('/messages', {
  action() {
    mount(Main, {
      content: (<MessagesContainer />)
    })
  },
  name: "messages"
});
//NOT NEEDED
*/
/*FlowRouter.route('/verify-email/:token', {
  action(params) {
    Accounts.verifyEmail(params.token, (error) => {
      if (error) {
        console.log(error);
      } else {
        FlowRouter.go("home");
        console.log("Verification Successful");
      }
    })
  },
  name: "verify-email"
});*/
