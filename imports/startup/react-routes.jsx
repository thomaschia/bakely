import React, { Component } from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import MainNav from '../ui/layouts/MainNav.jsx';
import MainNoNav from '../ui/layouts/MainNoNav.jsx';
import HomePageContainer from '../ui/components/HomePageContainer.jsx';
import ShopsHome from '../ui/components/ShopsHome.jsx';
import Shop from '../ui/components/Shop.jsx';
import AboutUs from '../ui/components/AboutUs.jsx';
import Account from '../ui/components/Account.jsx';
import ShopDashboard from '../ui/components/ShopDashboard.jsx';
import RegLogin from '../ui/components/RegLogin.jsx';
import ForgotLogin from '../ui/components/ForgotLogin.jsx';
import ResetLogin from '../ui/components/ResetLogin.jsx';
import SearchResults from '../ui/components/SearchResults.jsx';
import ProductDetailsComponent from '../ui/components/ProductDetailsComponent.jsx';
import NewProductContainer from '../ui/components/NewProductContainer.jsx';
import NewProductImageContainer from '../ui/components/NewProductImageContainer.jsx';
import EditProductComponent from '../ui/components/EditProductComponent.jsx';
import EditProductImageContainer from '../ui/components/EditProductImageContainer.jsx';
import NewShopContainer from '../ui/components/NewShopContainer.jsx';
import NewShopImageContainer from '../ui/components/NewShopImageContainer.jsx';
import EditShopImageContainer from '../ui/components/EditShopImageContainer.jsx';
import EditCoverImageContainer from '../ui/components/EditCoverImageContainer.jsx';
import ContactPage from '../ui/components/ContactPage.jsx';
import FAQ from '../ui/components/FAQ.jsx';
import DashboardContainer from '../ui/components/DashboardContainer.jsx';

Meteor.startup( () => {
  render(
    <Router history={ browserHistory }>
      <Route path='/' component={ MainNav }>
        <IndexRoute component={ HomePageContainer } />
        <Route path='/all-shops' component={ ShopsHome } />
        <Route path='/shops/:id' component={ Shop } />
        <Route path='/about-us' component={ AboutUs } />
        <Route path='/faq' component={ FAQ } />
        <Route path='/login' component={ RegLogin } />
        <Route path='/forgot-login' component={ ForgotLogin } />
        <Route path='/search-products/:query' component={ SearchResults } />
        <Route path='/product-details/:id' component = { ProductDetailsComponent } />
        <Route path='/contact/:id' component = { ContactPage } />
      </Route>
      <Route component={ MainNoNav }>
        <Route path='/my-account' component={ Account } />
        <Route path='/new-shop/:id' component={ NewShopContainer } />
        <Route path='/new-shop-image/:id' component={ NewShopImageContainer } />
        <Route path='/edit-shop-image/:id' component={ EditShopImageContainer } />
        <Route path='/edit-cover-pic/:id' component={ EditCoverImageContainer } />
        <Route path='/my-shop' component={ ShopDashboard } />
        <Route path='/new-product/:product_id' component={ NewProductContainer } />
        <Route path='/new-product-image/:id' component={ NewProductImageContainer } />
        <Route path='/edit-product/:id' component={ EditProductComponent } />
        <Route path='/edit-product-image/:id' component={ EditProductImageContainer } />
        <Route path='/reset-password/:token' component={ ResetLogin } />
        <Route path='/dashboard' component={ DashboardContainer } />
      </Route>
    </Router>,
    document.getElementById('react-root')
  );
});
